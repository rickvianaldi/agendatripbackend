<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationPackageTagsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vacation_package_tags', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('tag_id')->unsigned();
			$table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');

			$table->integer('vacation_package_id')->unsigned();
			$table->foreign('vacation_package_id')->references('id')->on('vacation_packages')->onDelete('cascade');

			$table->float('value')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vacation_package_tags',function(Blueprint $table){
			$table->dropForeign('vacation_package_tags_vacation_package_id_foreign');
			$table->dropForeign('vacation_package_tags_tag_id_foreign');
		});
		Schema::dropIfExists('vacation_package_tags');
	}

}
