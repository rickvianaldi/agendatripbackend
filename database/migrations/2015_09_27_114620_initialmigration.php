<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initialmigration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->string('email',100)->unique();
			$table->string('password', 256);
			$table->string('firstname',40);
			$table->string('lastname',40);
			$table->string('image',100);
			$table->string('app_key',100);
      $table->timestamps();
      $table->softDeletes();

		});

		Schema::create('tags', function($table)
		{
			$table->increments('id')->unique();
			$table->string('name');
		});


		Schema::create('user_tags', function($table)
		{
			$table->increments('id')->unsigned();

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

			$table->integer('tag_id')->unsigned();
			$table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');

			$table->unique(array('user_id','tag_id'));
			$table->double('value')->default(0);
		});

		Schema::create('vacation_sites', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('address');
			$table->string('contactnumber');
			$table->longtext('description');
			$table->double('latitude');
			$table->double('longitude');
			$table->double('rating')->unsigned();
			$table->string('image');
		});

		Schema::create('vacation_site_photos',function($table){
			$table->increments('id');

			$table->integer('vacation_site_id')->unsigned();
			$table->foreign('vacation_site_id')->references('id')->on('vacation_sites')->onDelete('cascade');

			$table->string('imageurl');
			$table->timestamps();
		});

		Schema::create('user_wishes', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('vacation_site_id')->unsigned();
			$table->foreign('vacation_site_id')->references('id')->on('vacation_sites')->onDelete('cascade');
            $table->timestamps();
		});


		Schema::create('vacation_site_tags', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('tag_id')->unsigned(); //semua foreign key wajib unsigned -docs
			$table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');

			$table->integer('vacation_site_id')->unsigned();
			$table->foreign('vacation_site_id')->references('id')->on('vacation_sites')->onDelete('cascade');

			$table->float('value')->default(0);
		});

		Schema::create('agendas', function($table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('name');
			$table->timestamp('start');
			$table->timestamp('end');
		});

		Schema::create('agenda_details', function($table)
		{
			$table->increments('id');

			$table->integer('agenda_id')->unsigned();
			$table->foreign('agenda_id')->references('id')->on('agendas')->onDelete('cascade');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

			$table->integer('vacation_site_id')->unsigned();
			$table->foreign('vacation_site_id')->references('id')->on('vacation_sites')->onDelete('cascade');

			$table->timestamp('start');
			$table->timestamp('end');
		});

		Schema::create('transports',function($table)
		{
			$table->increments('id');
			$table->string('name');
		});

		Schema::create('transport_details', function($table)
		{
			$table->integer('source_site_id')->unsigned();
			$table->foreign('source_site_id')->references('id')->on('agenda_details')->onDelete('cascade');

			$table->integer('destination_site_id')->unsigned();
			$table->foreign('destination_site_id')->references('id')->on('agenda_details')->onDelete('cascade');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

			$table->integer('transport_id')->unsigned();
			$table->foreign('transport_id')->references('id')->on('transports')->onDelete('cascade');

			$table->integer('estimated_expense')->unsigned()->default(0);
			$table->integer('estimated_time')->unsigned()->default(0);
		});

		Schema::create('vacation_packages',function($table){
			$table->increments('id')->unsigned();
			$table->double('price')->unsigned()->nullable(false);
			$table->integer('quota')->unsigned()->nullable(false);
			$table->string('title')->nullable(false);
			$table->string('header');
			$table->timestamp('sale_start')->nullable(false);
			$table->timestamp('sale_end')->nullable(false);
			$table->timestamp('date_start')->nullable(false);
			$table->timestamp('date_end')->nullable(false);
			$table->string('facility_inclusion');
			$table->string('facility_exclusion');
			$table->longtext('itinerary');
			$table->longtext('notes');
			$table->longtext('terms');
			$table->string('image');
			$table->timestamps();
		});

		Schema::create('vacation_package_purchases',function($table){
			$table->increments('id')->unsigned();

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('no action');

			$table->integer('vacation_package_id')->unsigned();
			$table->foreign('vacation_package_id')->references('id')->on('vacation_packages')->onDelete('no action');

			$table->integer('order_amount')->unsigned()->nullable(false);
			$table->string('status')->nullable(false)->default("unpaid");

			$table->string('reference_code')->nullable(true);
			$table->timestamps();
		});

		Schema::create('user_bookings',function($table){
			$table->increments('id');

			$table->integer('user_id')->unsigned()->nullable(true);
			$table->foreign('user_id')->references('id')->on('users')->onDelete('no action');

			$table->integer('agenda_id')->unsigned()->nullable(true);
			$table->foreign('agenda_id')->references('id')->on('agendas')->onDelete('no action');

			$table->integer('order_id')->unsigned()->nullable(false);
			$table->integer('order_detail_id')->unsigned()->nullable(false);
			$table->string('status')->nullable(false)->default('unpaid');
			$table->string('contact_email')->nullable(false);
			$table->string('order_type')->nullable(false);
			$table->double('order_amount')->nullable(false);
			$table->timestamp('order_expiration')->nullable(false);
			$table->string('token')->nullable(false);
			$table->timestamps();
		});



		// table untuk research
		Schema::create('user_collect_data',function($table){
			$table->increments('id')->unsigned();
			$table->string('username');
		});

		Schema::create('user_collect_data_response',function($table){
			$table->integer('id')->references('id')->on('user_collect_data')->onDelete('cascade');
			$table->integer('vacation_site_id')->references('id')->on('vacation_sites')->onDelete('no action');;
			$table->boolean('like');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{	//maintenance table
		// Schema::table('user_collect_data_response',function(Blueprint $table){
		// $table->dropForeign('user_collect_data_response_id_foreign');
		// $table->dropForeign('user_collect_data_response_vacation_site_id_foreign');
		// });


		Schema::table('user_bookings',function(Blueprint $table){
			$table->dropForeign('user_bookings_user_id_foreign');
			$table->dropForeign('user_bookings_agenda_id_foreign');
		});
		Schema::table('user_tags',function(Blueprint $table){
	      $table->dropForeign('user_tags_user_id_foreign');
	      $table->dropForeign('user_tags_tag_id_foreign');
	  	});
  	Schema::table('user_wishes',function(Blueprint $table){
      $table->dropForeign('user_wishes_user_id_foreign');
  	});
		Schema::table('agendas',function(Blueprint $table){
      $table->dropForeign('agendas_user_id_foreign');
		});
		Schema::table('agenda_details',function(Blueprint $table){
      $table->dropForeign('agenda_details_agenda_id_foreign');
      $table->dropForeign('agenda_details_vacation_site_id_foreign');
      $table->dropForeign('agenda_details_user_id_foreign');
  	});
    Schema::table('vacation_site_tags',function(Blueprint $table){
	    $table->dropForeign('vacation_site_tags_tag_id_foreign');
	    $table->dropForeign('vacation_site_tags_vacation_site_id_foreign');
    });
    Schema::table('transport_details',function(Blueprint $table){
	    $table->dropForeign('transport_details_source_site_id_foreign');
	    $table->dropForeign('transport_details_destination_site_id_foreign');
	    $table->dropForeign('transport_details_transport_id_foreign');
	    $table->dropForeign('transport_details_user_id_foreign');
  	});
  	Schema::table('vacation_site_photos',function(Blueprint $table){
	  	$table->dropForeign('vacation_site_photos_vacation_site_id_foreign');
  	});
    Schema::table('vacation_package_purchases',function(Blueprint $table){
	    $table->dropForeign('vacation_package_purchases_user_id_foreign');
	    $table->dropForeign('vacation_package_purchases_vacation_package_id_foreign');
    });

    Schema::dropIfExists('user_collect_data');
    Schema::dropIfExists('user_collect_data_response');

    Schema::dropIfExists('user_bookings');
    Schema::dropIfExists('vacation_site_photos');
    Schema::dropIfExists('vacation_package_purchases');
    Schema::dropIfExists('vacation_packages');
  	Schema::dropIfExists('transports');
	Schema::dropIfExists('transport_details');
	Schema::dropIfExists('agenda_details');
	Schema::dropIfExists('agendas');
	Schema::dropIfExists('user_wishes');
	Schema::dropIfExists('vacation_site_tags');
    Schema::dropIfExists('vacation_site_images');
    Schema::dropIfExists('vacation_site_photos');
	Schema::dropIfExists('vacation_sites');
	Schema::dropIfExists('users');
	Schema::dropIfExists('user_tags');
	Schema::dropIfExists('tags');
	Schema::dropIfExists('user_bookings');
	}

}
