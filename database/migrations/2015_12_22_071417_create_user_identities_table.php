<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserIdentitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_identities', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('title')->nullable(false);
			$table->string('first_name')->nullable(false);
			$table->string('last_name')->nullable(false);
			$table->timestamp('birthdate')->nullable(false);
			$table->string('phone_number')->nullable(false);
			$table->string('email')->nullable(true)->default('');
			$table->string('id_number')->nullable(true)->default('');
			$table->string('passport_number')->nullable(true)->default('');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_identities',function(Blueprint $table){
			$table->dropForeign('user_identities_user_id_foreign');
		});
		Schema::dropIfExists('user_identities');
	}

}
