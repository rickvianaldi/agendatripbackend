<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationPackageViewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vacation_package_views',function(Blueprint $table ){
			$table->increments('id');
			$table->integer('vacation_package_id')->unsigned();
			$table->foreign('vacation_package_id')->references('id')->on('vacation_packages')->onDelete('cascade');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('no action');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vacation_package_views',function(Blueprint $table){
			$table->dropForeign('vacation_package_views_vacation_package_id_foreign');
			$table->dropForeign('vacation_package_views_user_id_foreign');
		});
		Schema::dropIfExists('vacation_package_views');
	}

}
