<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_payments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('order_type');
			$table->string('order_id');
			$table->double('order_amount');
			$table->string('transaction_id');
			$table->string('transaction_time');
			$table->string('transaction_status');
			$table->string('payment_type');
			$table->string('payment_currency');
			$table->string('signature_key');
			$table->timestamps();
			$table->index('transaction_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_payments');
	}

}
