@extends('payment')

@section('content')
<h2>Payment Success</h2>
<div class="main-divider"></div>
<p>Your payment has been received. Thank you for choosing Agenda Trip.</p>
@endsection