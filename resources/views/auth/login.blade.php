@extends('app')

@section('content')
<div class="splash-image"></div>
<div class="container-fluid">
<div class="row">
	<img class="logo center-block" src="{{url('/assets/agenda-trip-logo-noshadow.png')}}"></img>
</div>
	<div class="row login-row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default login-panel">
				<div class="inner-panel">
					<div class="panel-heading text-center">Admin Panel</div>
					<div class="main-divider"></div>
								<div class="panel-body">
									@if (count($errors) > 0)
										<div class="alert alert-danger">
											<strong>Whoops!</strong> There were some problems with your input.<br><br>
											<ul>
												@foreach ($errors->all() as $error)
													<li>{{ $error }}</li>
												@endforeach
											</ul>
										</div>
									@endif
				
									<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
				
										<div class="form-group">
											<label class="col-md-4 control-label">E-Mail Address</label>
											<div class="col-md-7">
												<input type="email" class="form-control" name="email" value="{{ old('email') }}">
											</div>
										</div>
				
										<div class="form-group">
											<label class="col-md-4 control-label">Password</label>
											<div class="col-md-7">
												<input type="password" class="form-control" name="password">
											</div>
										</div>
				
										<div class="form-group">
											<div class="col-md-6 col-md-offset-4">
												<button type="submit" class="btn btn-primary">Login</button>
				
												<!-- <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a> -->
											</div>
										</div>
									</form>
								</div>
						<div class="copyright text-center">Copyright @ Agenda Trip 2015</div>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection
