@extends('master')

@section('title')
Agenda - {{$agendatitle}}
@stop


@section('content')


<div class="container">
	<h3 class="text-center">Agenda - {{$agendatitle}}</h3>
	<Br>
	<br>

	<?php $i = 1
	?>

	@foreach ($agendadetails as $agendadetail)
		
			<div class="single-agenda row">
						<div class="col-md-1 col-xs-1"><h4>{{$i}}.</h4></div>
					
						<div class="col-md-6">
								{{$agendadetail->vacation_site_id}}<br>
								{{date('d M y H:i',strtotime($agendadetail->start))}}<br>
								{{date('d M y H:i',strtotime($agendadetail->end))}}
						</div>
						
			</div>
	
		
		<Br>
		<?php $i++ ?>
						@if($i>1) 
						<div>transport detail</div>
						@endif
		<Br>
	@endforeach



</div>

	
@endsection