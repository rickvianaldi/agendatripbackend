@extends('master')

@section('title')
User Data{{$userdata['email']}} - {{Auth::User()->name}}
@stop


@section('content')
<div class="container caption">
	<div class="col-sm-6"><h1>EDIT User
	</h1>
	</div>
</div>

<div class="container vertical-padding30">		



	@if ($errors->any())	
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach

		</ul>
	@endif

			{!! Form::model($userdata,['class'=>'form-horizontal','method'=>'POST','autocomplete'=>'off'])!!}
			<div class="form-group">
			    <label for="userid" class="col-sm-2 control-label">user ID</label>
			    <label for="userid" class="col-sm-6 control-label" style="text-align:left">{{$userdata->id}}</label>
			 </div>

			<div class="form-group">
			    <label for="email" class="col-sm-2 control-label">email</label>
			    <div class="col-sm-6">
			    	{!! Form::email('email',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="Password" class="col-sm-2 control-label">Password</label>
			    <div class="col-sm-6">
			    	{!! Form::password('password',['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="firstname" class="col-sm-2 control-label">firstname</label>
			    <div class="col-sm-6">
			    	{!! Form::text('firstname',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">lastname</label>
			    <div class="col-sm-6">
			    	{!! Form::text('lastname',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label"></label>
			    <div class="col-sm-6">
			    	<input class="btn btn-primary" type="submit" value="Update User"></input> 
			 	 </div>
			</div>
	
			{!! Form::close()!!}


</div>


@endsection