<!-- AdminMaster is located inside admin views -->
@extends('master') 


@section('title')
Backoffice - Welcome {{Auth::User()->name}}
@stop


@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body text-center">


					<Br>
						<h2> Admin Panel Menu</h2>
						<h4>Choose A Management Panel</h4>
						<Br>

					<div class="row">
								<div class="vertical-padding30 col-md-4">
								<a href="{{url()}}/admin/user"><button class="menuthumbbutton center-block"><h3>User</h3></button></a>
								</div>
								
								<div class="vertical-padding30 col-md-4">
								<a href="{{url()}}/admin/vacationsite"><button class="menuthumbbutton center-block"><h3>Vacation Site</h3></button></a>
								</div>

								<div class="vertical-padding30 col-md-4">
								<a href="{{url()}}/admin/tag"><button class="menuthumbbutton center-block"><h3>Tag </h3></button></a>
								</div>
					</div>
					<div class="row">

								<div class="vertical-padding30 col-md-4">
								<a href="{{url()}}/admin/bookings"><button class="menuthumbbutton center-block"><h3>Bookings<br>(Transaction History)</h3></button></a>
								</div>

								<div class="vertical-padding30 col-md-4">
								<a href="{{url()}}/admin/vacationpackage"><button class="menuthumbbutton center-block"><h3>Vacation Package </h3></button></a>
								</div>
					</div>

							
				</div>
			</div>
		</div>
	</div>
</div>




@endsection
