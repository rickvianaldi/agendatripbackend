@extends('master')

@section('title')
Bookings
@stop


@section('content')
<div class="container">
	<h3 class="text-center">User Bookings <br>{{$username}}</h3>
	<br><br>
		
	<!-- begin pagination -->
	<?php 
			if(empty($_GET['status']))
				{$status="paid";}
			else
				{$status=$_GET['status'];}

			if(empty($_GET['perpage']))
				{$perpage="10";}
			else
				{$perpage=$_GET['perpage'];}
	?>
	<div class="pagination-container">{!!$bookinglist->appends(['perpage'=>$bookinglist->perpage(),'status'=>$showoptions['status']])->render()!!}
		<div class="pull-right">
		Per Page :
			<select class="select-pagination" onchange="location = this.options[this.selectedIndex].value;">
				<?php $pageoptions = [10,20,30,50,100];
					foreach($pageoptions as $pageoption){
						echo('
							<option 
							');
						if ($bookinglist->perpage() == $pageoption){echo('selected');}
						echo('
							value="
								?perpage='.$pageoption.'&status='.$status.'
								
							">'.$pageoption.'</option>
							');
					}
				?>
			</select>

		Show status:
			<select class="select-pagination" onchange="location = this.options[this.selectedIndex].value;">
				<?php $pageoptions = ['paid','unpaid','all'];
					foreach($pageoptions as $pageoption){
						echo('
							<option 
							');
						if ($showoptions['status'] == $pageoption){echo('selected');}
						echo('
							value="
								?perpage='.$perpage.'&status='.$pageoption.'
								
							">'.$pageoption.'</option>
							');
					}
				?>
			</select>
		</div>
	</div>
	<!-- end of page options -->

	<table class="table table-bordered table-striped">
		<tr>
		<!-- 	<td>actions</td> -->
			<td>ID</td>
			<td>Order ID</td>
			<td>status</td>
			<td>Contact email</td>
			<td>Type</td>
			<td>Amount in Rp.</td>
			<td>token</td>
			<td>Order created</td>
			<td>Expired</td>
			<td>Paid at</td>
		</tr>

		@if($bookinglist->isEmpty())
			<td colspan=10 class="text-center"><b>There are no bookings for this account</b></td>
		@else
			@foreach ($bookinglist as $booking)
				<tr> 
					<!-- <td width="90px">
					<a href="{{url('admin/user')}}/{{$booking->id}}"></a> 
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-delete-modal-sm" data-deleteid="{{$booking['id']}}" data-deleteemail="{{$booking['email']}}">delete</button>
					</td> -->
					<td>{{$booking['id']}}</td>
					<td>{{$booking['order_id']}}</td>
					<td>
						@if($booking['status'] == "unpaid" )
							<span style="color:red">
						@else	
							<span style="color:green">
						@endif
						{{ $booking['status'] }}
						</span>
					</td>
					<td>{{$booking['contact_email']}}</td>
					<td>{{$booking['order_type']}}</td>
					<td style="text-align:right;">{{number_format ($booking['order_amount'], 0, ',', '.')}}</td>
					<td>{{$booking['token']}}</td>
					<td>
						<b>{{ date('d F Y', strtotime($booking['created_at'])) }}</b><br>
						{{ date('H:i:s', strtotime($booking['created_at'])) }}
					</td>
					<td><b>{{ date('d F Y', strtotime($booking['order_expiration'])) }}</b><br>
						{{ date('H:i:s', strtotime($booking['order_expiration'])) }}
					</td>
					<td>
						@if($booking['status'] == "unpaid" )
							unpaid
						@else	
							<b>{{ date('d F Y', strtotime($booking['updated_at'])) }}</b><br>
							{{ date('H:i:s', strtotime($booking['updated_at'])) }}
						@endif
					</td>
				</tr>
			@endforeach
		@endif
	</table>
			<!-- prompt delete div -->
			<div class="modal fade bs-delete-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				
			  <div class="modal-dialog modal-sm">										
			    	<div class="modal-content">
				      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Prompt Delete</h4>
				      </div>
					      <div class="modal-body">
					      	
					        <p>Are you sure want to delete this user? <div id="username" class="bg-warning text-center"></div><br> This action cannot be undone!!</p>
					      </div>
				      <div class="modal-footer">
					        

					        <form action="{{url()}}/admin/userdelete" method="post">
					        	<input id="useridtodelete" type="hidden" name="id">
					        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					        	<input type="submit" class="btn btn-danger" value="Delete">
					        </form>
				      </div>
				    </div><!-- /.modal-content -->
			  </div>
			</div>
</div>
		<!-- prompt delete script -->
		<script language="javascript" type="text/javascript">
				$('.bs-delete-modal-sm').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal
				  var userid = button.data('deleteid')
				  var username = button.data('deleteemail') // Extract info from data-* attributes
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  var modal = $(this)
				  modal.find('.modal-title').text('Delete user: ' + username)
				  // document.getElementById("deleteidhere").setAttribute("href","{{url()}}/admin/user/delete/"+recipient) 
				  document.getElementById("useridtodelete").value = userid
				  document.getElementById("username").innerHTML = username
				})		 
	   </script>
	

@endsection