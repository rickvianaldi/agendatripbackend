@extends('master')

@section('title')
vacation Package list
@stop


@section('content')

<div class="container">
	<h3 class="text-center">Vacation Packages Management Panel</h3>

	<a href="{{url()}}/admin/vacationpackage/create"><button type="button" class="btn btn-primary">+ ADD NEW PACKAGE</button></a>
	{{$vacationpackages['searchstring']}}
	<br><br>
	<div class="row">
		<div class="col-sm-5">
					<div class="right-inner-addon">
					    <i class="glyphicon glyphicon-search"></i>
					    <form action="{{url()}}/admin/vacationpackage" method="GET">
						    <input type="search"
						           class="form-control" 
						           placeholder="Search Vacation Package Name"
						           onkeydown="if (event.keyCode == 13) { this.form.submit(); return false; }"
						           name="search"
						           value="{{$search['string']}}"
						           />
						</form>
					</div>
		</div>
	</div>
	
	<!-- begin pagination -->
	<div class="pagination-container">

		{!!$vacationpackages->appends(['perpage'=>$vacationpackages->perpage(),'search'=>$search['string']])->render()!!}
	
		<div class="pull-right">Per Page :
			<select class="select-pagination" onchange="location = this.options[this.selectedIndex].value;">
				<option <?php if ($vacationpackages->perpage() == 10){echo('selected');} ?> value="?perpage=10">10</option>
				<option <?php if ($vacationpackages->perpage() == 20){echo('selected');} ?> value="?perpage=20">20</option>
				<option <?php if ($vacationpackages->perpage() == 30){echo('selected');} ?> value="?perpage=30">30</option>
				<option <?php if ($vacationpackages->perpage() == 50){echo('selected');} ?> value="?perpage=50">50</option>
				<option <?php if ($vacationpackages->perpage() == 100){echo('selected');} ?> value="?perpage=100">100</option>
			</select>
		</div>
	</div>


		@foreach ($vacationpackages as $vacationpackage)
		<a href="{{url()}}/admin/vacationpackage/{{$vacationpackage->id}}">	
			<div class="vacation-package-single">
							<h4>
								<?php if($vacationpackage['sale_end'] < date('Y-m-d'))
									echo('<span class="expired-tagwrap">Sale expired</span >');
								?>
							{{$vacationpackage['title']}}</h4>
							{{$vacationpackage['header']}}
							<h5>Rp. {{number_format($vacationpackage['price'], 0, ',', '.')}}</h5>
							<div class="text-small">Created : {{date("d F Y",strtotime($vacationpackage['created_at']))}}
							<br>Edited : {{date("d F Y",strtotime($vacationpackage['updated_at']))}}</div>

							<div>@foreach($tags[$vacationpackage->id] as $tag)
						    	<div class="linear-tagwrap">{{$tag->tag['name']}}</div>
						    	@endforeach
					    	</div>

							<div style="padding-top:10px;">
							<a href="{{url()}}/admin/vacationpackage/edit/{{$vacationpackage->id}}" role="button" class="btn btn-primary">Edit</a>&nbsp;&nbsp;
							<button class="btn btn-danger" data-toggle="modal" data-target=".bs-delete-modal-sm" data-deleteid="{{$vacationpackage['id']}}" data-deletename="{{$vacationpackage['title']}}">Delete</a>

							</div>
			</div>
		</a>
		@endforeach


	<!-- prompt delete div -->
			<div class="modal fade bs-delete-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				
			  <div class="modal-dialog modal-sm">										
			    	<div class="modal-content">
				      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Prompt Delete</h4>
				      </div>
					      <div class="modal-body">
					      	
					        <p>Are you sure want to delete this? <div id="vacationpackagename" class="bg-warning text-center"></div><br> This action cannot be undone!!</p>
					      </div>
				      <div class="modal-footer">
					        

					        <form action="{{url()}}/admin/vacationpackagedelete" method="post">
					        	<input id="vacationpackageidtodelete" type="hidden" name="id">
					        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					        	<input type="submit" class="btn btn-danger" value="Delete">
					        </form>
				      </div>
				    </div><!-- /.modal-content -->
			  </div>
			</div>
	<!-- end of promt delete -->

</div>


<!-- prompt delete script -->
	<script language="javascript" type="text/javascript">
				$('.bs-delete-modal-sm').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal
				  var vacationpackageid = button.data('deleteid')
				  var vacationpackagename = button.data('deletename') // Extract info from data-* attributes
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  var modal = $(this)
				  modal.find('.modal-title').text('Delete vacation package: ' + vacationpackagename)
				  // document.getElementById("deleteidhere").setAttribute("href","{{url()}}/admin/vacation/delete/"+recipient) 
				  document.getElementById("vacationpackageidtodelete").value = vacationpackageid
				  document.getElementById("vacationpackagename").innerHTML = vacationpackagename
				})		 
	   </script>
@endsection