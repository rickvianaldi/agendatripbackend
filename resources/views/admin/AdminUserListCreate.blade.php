@extends('master')

@section('title')
Userlist - {{Auth::User()->name}}
@stop


@section('content')
	<div class="container-fluid col-lg-12 caption">
	<div class="col-sm-6"><h1>Create New User
	</h1>
	</div>
	</div>


	@if ($errors->any())	
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach

		</ul>
	@endif

	<form class="form-horizontal" action="{{url('admin/usercreate')}}" method="post" autocomplete="off">
			@include('admin._userform',['submit_button'=>'Create User'])
	</form>




@endsection