			<div class="form-group">
			    <label for="email" class="col-sm-2 control-label">email</label>
			    <div class="col-sm-6">
			    	{!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Email'])!!}
			 	 </div>
			 </div>

			<div class="form-group">
			    <label for="inputUsername3" class="col-sm-2 control-label">Password</label>
			    <div class="col-sm-6">
			    	{!! Form::password('password',['class'=>'form-control','placeholder'=>'Password'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="inputUsername3" class="col-sm-2 control-label">firstname</label>
			    <div class="col-sm-6">
			    	{!! Form::text('firstname',null,['class'=>'form-control','placeholder'=>'First name'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="inputUsername3" class="col-sm-2 control-label">lastname</label>
			    <div class="col-sm-6">
			    	{!! Form::text('lastname',null,['class'=>'form-control','placeholder'=>'Last Name'])!!}
			 	 </div>
			</div>

				<div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label"></label>
			    <div class="col-sm-6">
			    	<input class="btn btn-primary" type="submit" value="{{$submit_button}}"></input>
			 	 </div>
			</div>
