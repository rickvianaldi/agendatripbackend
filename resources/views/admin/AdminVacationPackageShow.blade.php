@extends('master')

@section('title')
{{$vacationpackage['title']}} - {{Auth::User()->name}}
@stop


@section('content')
<div class="container">
	<table class="table table-bordered">
		<tr>
			<td>ID</td>
			<td>{{$vacationpackage['id']}}</td>
		</tr>
		<tr>
			<td>Price</td>
			<td>{{$vacationpackage['price']}}</td>
		</tr>
		<tr>
			<td>title</td>
			<td>{{$vacationpackage['title']}}</td>
		</tr>

		<tr>
			<td>quota</td>
			<td>{{$vacationpackage['quota']}}</td>
		</tr>
		<tr>
			<td>header</td>
			<td>{{$vacationpackage['header']}}</td>
		</tr>

		<tr>
			<td>Tags</td>
			<td> 	
			@foreach($tags as $tag)
			<div class="tagwrap" style="max-width:150px;">{{$tag->tag->name}}</div>
			@endforeach
				    	</td>
		</tr>
		<tr>
			<td>Periode Pembelian</td>
			<td><div class="btn btn-primary">{{date("d F Y",strtotime($vacationpackage['sale_start']))}}</div> s/d <div class="btn btn-primary">{{date("d F Y",strtotime($vacationpackage['sale_end']))}}</div>
					<?php if($vacationpackage['sale_end'] < date('Y-m-d'))
									echo('&nbsp;&nbsp;<span class="expired-tagwrap">Sale expired</span >');
								?>
			</td>
		</tr>
	
		<tr>
			<td>Tanggal Berangkat & Pulang</td>
			<td><div class="btn btn-primary">{{date("d F Y",strtotime($vacationpackage['date_start']))}}</div> s/d <div class="btn btn-primary">{{date("d F Y",strtotime($vacationpackage['date_end']))}}</div></td>
		</tr>
	
		<tr>
			<td>facility_inclusion</td>
			<td>{!!$vacationpackage['facility_inclusion']!!}</td>
		</tr>
		<tr>
			<td>facility_exclusion</td>
			<td>{!!$vacationpackage['facility_exclusion']!!}</td>
		</tr>
		<tr>
			<td>itinerary</td>
			<td>{!!$vacationpackage['itinerary']!!}</td>
		</tr>
		<tr>
			<td>notes</td>
			<td>{!!$vacationpackage['notes']!!}</td>
		</tr>
		<tr>
			<td>terms</td>
			<td>{!!$vacationpackage['terms']!!}</td>
		</tr>

	</table>



</div>	
@endsection