@extends('master')

@section('title')
Agenda - {{$name}}
@stop


@section('content')


<div class="container">
	<h3 class="text-center">Agendas of {{$name}}</h3>
	<Br>
	<br>

	<?php $i = 1
	?>

	@foreach ($agendas as $agenda)
		<a href="{{url()}}/admin/agendadetail/{{$agenda->id}}">
			<div class="single-agenda row">
						<div class="col-md-1 col-xs-1"><h4>{{$i}}.</h4></div>
					
						<div class="col-md-6"><h4>{{$agenda->name}}</h4>
								<div><span style="font-size:1.3em;">
									<b>{{date('d',strtotime($agenda->start))}}
									{{date('M',strtotime($agenda->start))}}</b>
									</span>
									{{date('Y',strtotime($agenda->start))}} -
								<b>{{date('H:i',strtotime($agenda->start))}}</b>
								</div>

								<div><span style="font-size:1.3em;">
									<b>{{date('d',strtotime($agenda->end))}}
									{{date('M',strtotime($agenda->end))}}</b>
									</span>

									{{date('Y',strtotime($agenda->end))}} -

									<b>{{date('H:i',strtotime($agenda->end))}}</b>
								</div>
								<div>
									<?php 
									    $datetime1 = new DateTime(($agenda->start));
									    $datetime2 = new DateTime(($agenda->end));
									    $interval = $datetime1->diff($datetime2);
									?>
									duration : 
									@if( (int)$interval->format('%d') >= 1)
									{{$interval->format('%a day(s)')}}
									@elseif ( (int)$interval->format('%h') >= 1)
									{{$interval->format('%h hour(s)')}}
									@else
									{{$interval->format('%i minute(s)')}}
									@endif
								</div>
						</div>
						<div class="col-md-4">
						</div>
						<div class="col-md-1">
							<form action="{{url()}}/admin/agendadelete" method="post">
					        	<input id="agendaidtodelete" type="hidden" name="9"> <!-- {{$agenda->id}} -->
					 
					        </form>
						</div>


			</div>
		</a>
		
		<Br>
		<?php $i++ ?>
	@endforeach



</div>

	
@endsection