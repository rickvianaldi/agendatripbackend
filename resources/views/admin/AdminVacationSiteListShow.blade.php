@extends('master')

@section('title')
vacationlist - {{Auth::user()->name}}
@stop


@section('content')

<div class="container">
	<h3 class="text-center">Vacation Site Management Panel</h3>

	<a href="{{url()}}/admin/vacationsite/create"><button type="button" class="btn btn-primary">+ Create Site</button></a>
	<br><br>

	<div class="row">
		<div class="col-sm-5">
					<div class="right-inner-addon">
					    <i class="glyphicon glyphicon-search"></i>
					    <form action="{{url()}}/admin/vacationsite" method="get">
						    <input type="search"
						           class="form-control" 
						           placeholder="Search vacation site"
						           onkeydown="if (event.keyCode == 13) { this.form.submit(); return false; }"
						           name="search"
						           value="{{$search['string']}}" 
						           />
						</form>
					</div>
		</div>
	</div>

	<!-- begin pagination -->
	<div class="pagination-container">{!!$vacationlist->appends(['perpage'=>$vacationlist->perpage(),'search'=>$search['string']])->render()!!}
		<div class="pull-right">Per Page :
		<select class="select-pagination" onchange="location = this.options[this.selectedIndex].value;">
			<option <?php if ($vacationlist->perpage() == 10){echo('selected');} ?> value="?perpage=10">10</option>
			<option <?php if ($vacationlist->perpage() == 20){echo('selected');} ?> value="?perpage=20">20</option>
			<option <?php if ($vacationlist->perpage() == 30){echo('selected');} ?> value="?perpage=30">30</option>
			<option <?php if ($vacationlist->perpage() == 50){echo('selected');} ?> value="?perpage=50">50</option>
			<option <?php if ($vacationlist->perpage() == 100){echo('selected');} ?> value="?perpage=100">100</option>
		</select>
		</div>
	</div>


	<table class="table table-bordered table-striped">
		<tr>
			<td>Action</td>	
			<td>Image</td>
			<td>ID</td>
			<td>NAME</td>
			<td>adresss</td>
			<td width="200px">Tags</td>
			<td>contact number</td>
			<td></td>
			
		</tr>
		@foreach ($vacationlist as $vacationsite)
			<tr> 
				<td width="150px">
				<a href="{{url('admin/vacationsite/edit')}}/{{$vacationsite->id}}"><button type="button" class="btn btn-primary">edit</button> </a> 
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-delete-modal-sm" data-deleteid="{{$vacationsite['id']}}" data-deleteemail="{{$vacationsite['name']}}">delete</button> </td>
				<td>
					@if($vacationsite->image)
						<a href="{{url()}}/vacationimages/{{$vacationsite->image}}" data-lightbox="{{$vacationsite->id}}" title="{{$vacationsite->name}}">
							<img style="width:100px;height:100px;"src="{{url()}}/small_vs_thumbs/ts_{{$vacationsite->image}}"></img>
						</a>
					@else
						<div style="width:100px;height:100px;display:table-cell;vertical-align:middle;"><p class="text-center">no<br>photo</p></div>
					@endif
				</td>
				<td>{{$vacationsite->id}}</td>
				<td>{{$vacationsite->name}}</td>
				
				<td>{{$vacationsite->address}}</td>
				<td>
					@foreach($tags[$vacationsite->id] as $tag)
			    	<div class="tagwrap">{{$tag->tag['name']}}</div>
			    	@endforeach
			    </td>
				<td width="120px">{{$vacationsite->contactnumber}}</td>
				<td width="120px"><a href="{{url('admin/vacationsite')}}/{{$vacationsite->id}}"><button class="btn btn-primary">See details</button></a></td>
				
			</tr>
		@endforeach
	</table>

	<!-- prompt delete div -->
			<div class="modal fade bs-delete-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				
			  <div class="modal-dialog modal-sm">										
			    	<div class="modal-content">
				      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Prompt Delete</h4>
				      </div>
					      <div class="modal-body">
					      	
					        <p>Are you sure want to delete this? <div id="vacationname" class="bg-warning text-center"></div><br> This action cannot be undone!!</p>
					      </div>
				      <div class="modal-footer">
					        

					        <form action="{{url()}}/admin/vacationdelete" method="post">
					        	<input id="vacationidtodelete" type="hidden" name="id">
					        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					        	<input type="submit" class="btn btn-danger" value="Delete">
					        </form>
				      </div>
				    </div><!-- /.modal-content -->
			  </div>
			</div>
	<!-- end of promt delete -->
</div>


<!-- prompt delete script -->
	<script language="javascript" type="text/javascript">
				$('.bs-delete-modal-sm').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal
				  var vacationid = button.data('deleteid')
				  var vacationname = button.data('deleteemail') // Extract info from data-* attributes
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  var modal = $(this)
				  modal.find('.modal-title').text('Delete vacation: ' + vacationname)
				  // document.getElementById("deleteidhere").setAttribute("href","{{url()}}/admin/vacation/delete/"+recipient) 
				  document.getElementById("vacationidtodelete").value = vacationid
				  document.getElementById("vacationname").innerHTML = vacationname
				})		 
	   </script>
@endsection