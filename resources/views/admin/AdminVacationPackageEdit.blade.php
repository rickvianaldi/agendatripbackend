@extends('master')

@section('title')
EDIT {{$vacationpackage['name']}}
@stop


@section('content')

<!-- tag-it scripts -->
<script src="{{url()}}/js/vendor/jquery-ui.min.js"></script>
<script src="{{url()}}/js/vendor/tag-it.min.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="{{url()}}/js/vacationpackagescript.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<div class="container">
	<div class="col-sm-6"><h1>EDIT - {{$vacationpackage['title']}}
	</h1>
	<h4>Package ID : {{$vacationpackage['id']}}	</h4>
	</div>
</div>

	

<div class="container vertical-padding30">
	@if ($errors->any())	
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif

			{!! Form::model($vacationpackage,['class'=>'form-horizontal','method'=>'POST','autocomplete'=>'off'])!!}
			
			<div class="form-group">
			    <label for="title" class="col-sm-2 control-label">Title<span class="req">*</span></label>		 
			    	<div class="col-sm-6">
			    		{!! Form::text('title',null,['class'=>'form-control'])!!}
			    	</div>
			 </div>

			<div class="form-group">
			    <label for="quota" class="col-xs-12 col-sm-2 control-label">Quota<span class="req">*</span></label>
			    <div class="col-xs-3" style="width:200px;">
			    	{!! Form::input('number','quota',null,['class'=>'form-control'])!!}
			 	 </div>
			 	 <label class="col-xs-3 control-label">packages<span class="req">*</span></label>
			</div>

			<div class="form-group">
			    <label for="header" class="col-sm-2 control-label">Tag-Line Singkat<span class="req">*</span></label>
			    <div class="col-sm-6">
			    	{!! Form::text('header',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="Price" class="col-sm-2 control-label">Price Rp.<span class="req">*</span></label>
			    <div class="col-sm-6">
			    	{!! Form::input('number','price',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="tag" class="col-sm-2 control-label">Tag</label>
			    <div class="col-sm-6">
			    	<ul id="myTags">
				    <!-- Existing list items will be pre-added to the tags //modified, doesn't work-->
				    <!-- <li>Tag1</li> -->
					</ul>

					<input id="tagField" class="form-control" name="tag" type="hidden" value="
					   	@foreach($tags as $tag)
				    	{{$tag->tag->name}},
				    	@endforeach
					"></input>

					<div class="form-tips-text">Please use " , " to confirm certain tag name</div>
			 	 </div>
			</div>

			<div>Periode Reservasi</div>
			<div class="form-group">
			    <label for="sale_start" class="col-sm-2 control-label">Mulai<span class="req">*</span></label>
			    <div class="col-sm-6">
			    	{!! Form::input('text','sale_start',date("Y-m-j",strtotime($vacationpackage->sale_start)),['class'=>'datepicker form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="sale_end" class="col-sm-2 control-label">Berakhir<span class="req">*</span></label>
			    <div class="col-sm-6">
			    	{!! Form::input('text','sale_end',date("Y-m-j",strtotime($vacationpackage->sale_end)),['class'=>'datepicker form-control'])!!}
			 	 </div>
			</div>
			<Br>

		   <div>Tanggal Berangkat dan selesai</div>
			<div class="form-group">
			    <label for="sale_start" class="col-sm-2 control-label">Berangkat</label>
			    <div class="col-sm-6">
			    	{!! Form::input('text','date_start',date("Y-m-j",strtotime($vacationpackage->date_start)),['class'=>'datepicker form-control','id'=>'date_start','onchange'=>'changedate(this.value)'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="sale_end" class="col-sm-2 control-label">Selesai</label>
			    <div class="col-sm-6">
			    	{!! Form::input('text','date_end',date("Y-m-j",strtotime($vacationpackage->date_end)),['class'=>'datepicker form-control','id'=>'date_end'])!!}
			 	 </div>
			</div>
			<Br>

			<div>PENJELASAN JANGKAUAN PAKET</div>
			<div class="form-group">
			    <label for="facility_inclusion" class="col-sm-2 control-label">Termasuk<span class="req">*</span></label>
			    <div class="col-sm-6">
			    	{!! Form::textarea('facility_inclusion',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="facility_exclusion" class="col-sm-2 control-label">Tidak Termasuk</label>
			    <div class="col-sm-6">
			    	{!! Form::textarea('facility_exclusion',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="itinerary" class="col-sm-2 control-label">itinerary (susunan acara)</label>
			    <div class="col-sm-6">
			    	{!! Form::textarea('itinerary',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="notes" class="col-sm-2 control-label">Catatan<span class="req">*</span></label>
			    <div class="col-sm-6">
			    	{!! Form::textarea('notes',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="terms" class="col-sm-2 control-label">Terms<span class="req">*</span></label>
			    <div class="col-sm-6">
			    	{!! Form::textarea('terms',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group row">
			    <label class="col-sm-2 control-label"></label>
			    <div class="col-sm-6">
			    	<input class="btn btn-primary" type="submit" value="Update Package"></input>
			 	 </div>
			</div>

			{!! Form::close()!!}

			<!-- Reserved for further dev
			<div class="container">
	        <div class="dropzone" id="dropzoneFileUpload">  	
	        </div>
	   		</div>
 			-->
	
			<!-- <h3 class="text-center">Images of {{$vacationpackage->title}}</h3><br>
 -->
			<div class="row">

			</div>

		</div>


</div>
	
<script>
	// // dropzone script, reserved for further development
	// var baseUrl = "{{ url('/') }}";
 //        var token = "{{ Session::getToken() }}";

 //        Dropzone.autoDiscover = false;
 //        var myDropzone = new Dropzone("div#dropzoneFileUpload", { //options lined here
 //            url: baseUrl + "/admin/dropzone/UploadVacationPackageImages/{{$id}}",
 //            params: {
 //                _token: token
 //            },
 //            maxFilesize: 3,
 //           	dictFallbackMessage:"Your browser doesn't support upload image, please enable JS and JQUERY",
 //           	acceptedFiles:".jpg,.png,.jpeg,.gif,.bmp",
 //           	dictInvalidFileType:"You can only upload JPG, PNG, BMP, GIF files",
 //        });
</script>


@endsection


