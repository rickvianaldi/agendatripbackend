@extends('master')

@section('title')
tags - {{Auth::User()->name}}
@stop


@section('content')

<div class="container">

	<h3 class="text-center">Tag Management Panel</h3>
	<Br><br>


	<a href="{{url('admin/tagcreate')}}"><button class="btn btn-primary"> + Create New Tag</button></a>
	<br><br>
	<div class="row">
		<div class="col-sm-5">
					<div class="right-inner-addon">
					    <i class="glyphicon glyphicon-search"></i>
					    <form action="{{url()}}/admin/tag" method="get">
						    <input type="search"
						           class="form-control" 
						           placeholder="Search Tag Name"
						           onkeydown="if (event.keyCode == 13) { this.form.submit(); return false; }"
						           name="search"
						           value="{{$search['string']}}" 
						           />
						</form>
					</div>
		</div>
	</div>

	<!-- begin pagination -->
	<div class="pagination-container">{!!$tags->appends(['perpage'=>$tags->perpage(),'search'=>$search['string']])->render()!!}
		<div class="pull-right">Per Page :
		<select class="select-pagination" onchange="location = this.options[this.selectedIndex].value;">
			<option <?php if ($tags->perpage() == 10){echo('selected');} ?> value="?perpage=10">10</option>
			<option <?php if ($tags->perpage() == 20){echo('selected');} ?> value="?perpage=20">20</option>
			<option <?php if ($tags->perpage() == 30){echo('selected');} ?> value="?perpage=30">30</option>
			<option <?php if ($tags->perpage() == 50){echo('selected');} ?> value="?perpage=50">50</option>
			<option <?php if ($tags->perpage() == 100){echo('selected');} ?> value="?perpage=100">100</option>
		</select>
		</div>
	</div>

	

	<table class="table table-bordered table-striped">
		<tr>
			<td>action</td>
			<td>ID</td>
			<td>NAME</td>
		</tr>
		@foreach ($tags as $tag)
			<tr> 
				<td width="150px">
				<a href="{{url('admin/tag/edit')}}/{{$tag->id}}"><button type="button" class="btn btn-primary">edit</button> </a> 
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-delete-modal-sm" data-deleteid="{{$tag['id']}}" data-deletetagname="{{$tag['name']}}">delete</button> </td>

				<td>{{$tag->id}}</td>
				<td>{{$tag->name}}</td>			
			</tr>
		@endforeach
	</table>
</div>
		<!-- prompt delete div -->
		<div class="modal fade bs-delete-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			
		  <div class="modal-dialog modal-sm">										
		    	<div class="modal-content">
			      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Prompt Delete</h4>
			      </div>
				      <div class="modal-body">
				      	
				        <p>Are you sure want to delete this tag? <div id="tagname" class="bg-warning text-center"></div><br> This action cannot be undone!!</p>
				      </div>
			      <div class="modal-footer">
				        

				        <form action="{{url()}}/admin/tagdelete" method="post">
				        	<input id="tagidtodelete" type="hidden" name="id">
				        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				        	<input type="submit" class="btn btn-danger" value="Delete">
				        </form>
			      </div>
			    </div><!-- /.modal-content -->
		  </div>
		</div>

		<!-- prompt delete script -->
		<script language="javascript" type="text/javascript">
				$('.bs-delete-modal-sm').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal
				  var tagid = button.data('deleteid')
				  var tagname = button.data('deletetagname') // Extract info from data-* attributes
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  var modal = $(this)
				  modal.find('.modal-title').text('Delete tag: ' + tagname)
				  // document.getElementById("deleteidhere").setAttribute("href","{{url()}}/admin/tag/delete/"+recipient) 
				  document.getElementById("tagidtodelete").value = tagid
				  document.getElementById("tagname").innerHTML = tagname
				})		 
	   </script>
	

@endsection