@extends('master')

@section('title')
Create - {{Auth::User()->name}}
@stop


@section('content')

<script>
function initAutocomplete() {
var myLatLng = {lat: -6.175380870386229, lng:106.82714905589819 };

  var map = new google.maps.Map(document.getElementById('map'), {
    center: myLatLng,
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });


    //Create the marker.
            marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event){
                markerLocation(); });


  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];

  
  // Listen for the event fired when the user selects a prediction and retrieve
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
  // [END region_getplaces]

    google.maps.event.addListener(map, 'click', function(event) {                
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if(marker === false){
          
           
        } else{
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        //Get the marker's location.
        markerLocation();
    });
}

function markerLocation(){
    //Get location.
    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    document.getElementById('lat').value = currentLocation.lat(); //latitude
    document.getElementById('lng').value = currentLocation.lng(); //longitude
}
</script>

<script src="{{url()}}/js/vendor/jquery-ui.min.js"></script>
<script src="{{url()}}/js/vendor/tag-it.min.js"></script>

<div class="container">
<div class="col-sm-6"><h1>Create new site
</h1>
</div>
</div>

<!--  @if(Session::has('success'))
          <div class="alert-box success">
          <h2>{!! Session::get('success') !!}</h2>
          </div>
 @endif -->


<div class="container-fluid vertical-padding30">

	@if ($errors->any())	
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif


			{!! Form::open([
			'class'=>'form-horizontal',
			'method'=>'POST',
			'autocomplete'=>'off',
			'files'=>'true'
			])!!}
			<div class="form-group">
			    <label for="name" class="col-sm-2 control-label">Name</label>		 
			    	<div class="col-sm-6">
			    		{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Name of vacation site'])!!}
			    	</div>
			 </div>

			<div class="form-group">
			    <label for="address" class="col-sm-2 control-label">address</label>
			    <div class="col-sm-6">
			    	{!! Form::text('address',null,['class'=>'form-control','placeholder'=>'Address'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="contactnumber" class="col-sm-2 control-label">Contact Number</label>
			    <div class="col-sm-6">
			    	{!! Form::text('contactnumber',null,['class'=>'form-control','placeholder'=>'Example (021) 231 123'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="tag" class="col-sm-2 control-label">Tag</label>
			    <div class="col-sm-6">
			    	<ul id="myTags">
				    <!-- Existing list items will be pre-added to the tags //modified, doesn't work-->
				    <!-- <li>Tag1</li> -->
					</ul>

				<input id="tagField" class="form-control" name="tag" type="hidden"></input>

					<div class="form-tips-text">Please use " , " to confirm certain tag name</div>
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="description" class="col-sm-2 control-label">Description</label>
			    <div class="col-sm-6">
			    	{!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Description'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="description" class="col-sm-2 control-label"></label>
			    <div class="col-sm-8 col-md-6">
			      <h2>Choose Location</h2>
			    	<input id="pac-input" class="controls" type="text" placeholder="Search Box">
   						 <div id="map"></div>
   						 <div class="form-inline">
   							{!! Form::hidden('latitude',null,['class'=>'form-control','id'=>'lat'])!!} {!! Form::hidden('longitude',null,['class'=>'form-control','id'=>'lng'])!!}
				         </div>
			 	 </div>
			</div>

			<div class="form-group row">
			    <label class="col-sm-2 control-label"></label>
			    <div class="col-sm-6">
			    	<input class="btn btn-primary" type="submit" value="Create Vacation Site"></input>
			 	 </div>
			</div>
			{!! Form::close()!!}


			<div>note : you can upload photo in edit section of site after created</div>
</div>
			

<script type="text/javascript">
    $(document).ready(function() {
      
	

		//tag it stuff
		$("#myTags").tagit({
			allowSpaces:true,
			caseSensitive : false,
			 singleField: true,
             singleFieldNode: $('#tagField'),
               preprocessTag: function (val) {
			        if (!val) {
			            return '';
			        }
			        var values = val.split(",");
			        if (values.length > 1) {
			            for (var i = 0; i < values.length; i++) {
			                $("#myTags").tagit("createTag", values[i]);
			            }
			            return ''
			        } else {
			            return val
			        }
   				 }
		});

    });
</script>

@endsection