@extends('master')

@section('title')
{{$vacationsite['name']}} - {{Auth::User()->name}}
@stop


@section('content')

<div class="container">
	<table class="table table-bordered">
		<tr>
			<td>Name</td>
			<td>{{$vacationsite['name']}}</td>
		</tr>
		<tr>
			<td>Address</td>
			<td>{{$vacationsite['address']}}</td>
		</tr>
		<tr>
			<td>Contact Number</td>
			<td>{{$vacationsite['contactnumber']}}</td>
		</tr>

		<tr>
			<td>Rating</td>
			<td>{{$vacationsite['rating']}}</td>
		</tr>
		<tr>
			<td>Descriptions</td>
			<td>{{$vacationsite['description']}}</td>
		</tr>
		<tr>
			<td>Location</td>
			<td><div id="map" style="height:300px;width:400px;"></div></td>
		</tr>
	</table>



	<div class="row">
				<h3 class="text-center">Images of {{$vacationsite->name}}</h3><br>

				@if(count($vacationsitephotos) == 0)
					<div class="text-center">there is no photo yet, please upload above</div>
				@endif

				@foreach($vacationsitephotos as $photo)
				<div class="imagepane col-sm-4 col-md-3">
					<a href="{{url()}}/vacationimages/{{$photo->imageurl}}" data-lightbox="vacationimage" title="{{$vacationsite['name']}}"> 
						<img class="center-block" style="border-radius: 5px;" src="{{url()}}/moderate_vs_thumbs/tm_{{$photo->imageurl}}"></img>
					</a>
				</div>
				@endforeach

	</div>
</div>	


<script>
function initAutocomplete() {
var myLatLng = {lat: {{$vacationsite['latitude']}}, lng:{{$vacationsite['longitude']}}  };

  var map = new google.maps.Map(document.getElementById('map'), {
    center: myLatLng,
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });


    //Create the marker.
            marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                draggable: true //make it draggable
            });
}

</script>

 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpXVKb63gUuHIPTgTIkXiG-7qON9SI_ms&libraries=places&callback=initAutocomplete"
 async defer>
 </script>
@endsection