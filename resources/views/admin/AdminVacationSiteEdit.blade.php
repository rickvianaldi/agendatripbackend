@extends('master')

@section('title')
EDIT {{$vacationsite['name']}} - {{Auth::User()->name}}
@stop


@section('content')

<!-- tag-it scripts -->

<!-- perpage script below
has to be inline because need to use laravel form -->
<script>

</script>
        
<script src="{{url()}}/js/vendor/jquery-ui.min.js"></script>
<script src="{{url()}}/js/vendor/tag-it.min.js"></script>

<div class="container caption">
	<div class="col-sm-6"><h1>EDIT - {{$vacationsite['name']}}
	</h1>
	<h4>Site ID : {{$vacationsite['id']}}	</h4>
	</div>
</div>

	@if ($errors->any())	
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif


<div class="container vertical-padding30">
			{!! Form::model($vacationsite,['class'=>'form-horizontal','method'=>'POST','autocomplete'=>'off'])!!}
				<input type="hidden" name="id" value="{{$vacationsite['id']}}">
			<div class="form-group">
			    <label for="name" class="col-sm-2 control-label">Name</label>		 
			    	<div class="col-sm-6">
			    		{!! Form::text('name',null,['class'=>'form-control'])!!}
			    	</div>
			 </div>

			<div class="form-group">
			    <label for="address" class="col-sm-2 control-label">address</label>
			    <div class="col-sm-6">
			    	{!! Form::text('address',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="contactnumber" class="col-sm-2 control-label">Contact Number</label>
			    <div class="col-sm-6">
			    	{!! Form::text('contactnumber',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="tag" class="col-sm-2 control-label">Tag</label>
			    <div class="col-sm-6">
			    	<ul id="myTags">
				    <!-- Existing list items will be pre-added to the tags //modified, doesn't work-->
				    <!-- <li>Tag1</li> -->
					</ul>

				<input id="tagField" class="form-control" name="tag" type="hidden" value="
				   	@foreach($tags as $tag)
			    	{{$tag->tag['name']}},
			    	@endforeach
				"></input>

					<div class="form-tips-text">Please use " , " to confirm certain tag name</div>
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="description" class="col-sm-2 control-label">Description</label>
			    <div class="col-sm-6">
			    	{!! Form::textarea('description',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>


			<div class="form-group">
			    <label for="description" class="col-sm-2 control-label"></label>
			    <div class="col-sm-8 col-md-6">
			      <h2>Choose Location</h2>
			    	<input id="pac-input" class="controls" type="text" placeholder="Search Box">
   						 <div id="map"></div>
   						 <div class="form-inline">
   							{!! Form::hidden('latitude',null,['class'=>'form-control','id'=>'lat'])!!} {!! Form::hidden('longitude',null,['class'=>'form-control','id'=>'lng'])!!}
				         </div>
			 	 </div>
			</div>
		
						

			<div class="form-group row">
			    <label class="col-sm-2 control-label"></label>
			    <div class="col-sm-6">
			    	<input class="btn btn-primary" type="submit" value="Update Vacation Site"></input>
			 	 </div>
			</div>
			{!! Form::close()!!}

			<div class="container">
	        <div class="dropzone" id="dropzoneFileUpload">  	
	        </div>
	   		</div>

	
			<h3 class="text-center">Images of {{$vacationsite->name}}</h3><br>

			<div class="row">

				@if(count($vacationsitephotos) == 0)
					<div class="text-center">there is no photo yet, please upload above</div>
				@endif

				@foreach($vacationsitephotos as $photo)
				<form action="{{url()}}/admin/vacationsite/deletesingleimage" method="post">
					<div class="imagepane col-sm-4 col-md-3 text-center">
						<a href="{{url()}}/vacationimages/{{$photo->imageurl}}" data-lightbox="vacationimage" title="{{$vacationsite['name']}}"> 
							<img class="center-block" style="border-radius: 5px; border: 2px solid #F0F0F0;" src="{{url()}}/moderate_vs_thumbs/tm_{{$photo->imageurl}}"></img>
						</a>
						<input type="hidden" value="{{$photo->imageurl}}" name="filename">
						<input type="hidden" value="{{$vacationsite->id}}" name="vacationsiteid">
						<button class="btn btn-danger" type="submit">Delete Image</button>
					</div>
				</form>
				@endforeach

			</div>

		</div>


 

<script type="text/javascript">
    $(document).ready(function() {
       	 var baseUrl = "{{ url('/') }}";
			        var token = "{{ Session::getToken() }}";
			        
			        Dropzone.autoDiscover = false;
			        var myDropzone = new Dropzone("div#dropzoneFileUpload", { //options lined here
			            url: baseUrl + "/admin/dropzone/uploadFiles/{{$id}}",
			            params: {
			                _token: token
			            },
			            maxFilesize: 3,
			           	dictFallbackMessage:"Your browser doesn't support upload image, please enable JS and JQUERY",
			           	acceptedFiles:".jpg,.png,.jpeg,.gif,.bmp",
			           	dictInvalidFileType:"You can only upload JPG, PNG, BMP, GIF files",
			        });

	

		//tag it stuff
		$("#myTags").tagit({
			allowSpaces:true,
			caseSensitive : false,
			 singleField: true,
             singleFieldNode: $('#tagField'),
               preprocessTag: function (val) {
			        if (!val) {
			            return '';
			        }
			        var values = val.split(",");
			        if (values.length > 1) {
			            for (var i = 0; i < values.length; i++) {
			                $("#myTags").tagit("createTag", values[i]);
			            }
			            return ''
			        } else {
			            return val
			        }
   				 }
		});

    });
</script>


	<script>
   		function initAutocomplete() {
			var myLatLng = {lat: {{$vacationsite['latitude']}}, lng:{{$vacationsite['longitude']}}  };
			var map = new google.maps.Map(document.getElementById('map'), {
			    center: myLatLng,
			    zoom: 13,
			    mapTypeId: google.maps.MapTypeId.ROADMAP
			});


		    //Create the marker.
		            marker = new google.maps.Marker({
		                position: myLatLng,
		                map: map,
		                draggable: true //make it draggable
		            });

		            //Listen for drag events!
		            google.maps.event.addListener(marker, 'dragend', function(event){
		                markerLocation(); });

			// Create the search box and link it to the UI element.
			var input = document.getElementById('pac-input');
			var searchBox = new google.maps.places.SearchBox(input);
			map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

			// Bias the SearchBox results towards current map's viewport.
			map.addListener('bounds_changed', function() {
			  searchBox.setBounds(map.getBounds());
			});

			var markers = [];

		  
		  // Listen for the event fired when the user selects a prediction and retrieve
		  searchBox.addListener('places_changed', function() {
		    var places = searchBox.getPlaces();

		    if (places.length == 0) {
		      return;
		    }

		    // Clear out the old markers.
		    markers.forEach(function(marker) {
		      marker.setMap(null);
		    });
		    markers = [];

		    // For each place, get the icon, name and location.
		    var bounds = new google.maps.LatLngBounds();
		    places.forEach(function(place) {
		      var icon = {
		        url: place.icon,
		        size: new google.maps.Size(71, 71),
		        origin: new google.maps.Point(0, 0),
		        anchor: new google.maps.Point(17, 34),
		        scaledSize: new google.maps.Size(25, 25)
		      };

		      // Create a marker for each place.
		      markers.push(new google.maps.Marker({
		        map: map,
		        icon: icon,
		        title: place.name,
		        position: place.geometry.location
		      }));

		      if (place.geometry.viewport) {
		        // Only geocodes have viewport.
		        bounds.union(place.geometry.viewport);
		      } else {
		        bounds.extend(place.geometry.location);
		      }
		    });
		    map.fitBounds(bounds);
		  });
		  // [END region_getplaces]

		    google.maps.event.addListener(map, 'click', function(event) {                
		        //Get the location that the user clicked.
		        var clickedLocation = event.latLng;
		        //If the marker hasn't been added.
		        if(marker === false){
		          
		           
		        } else{
		            //Marker has already been added, so just change its location.
		            marker.setPosition(clickedLocation);
		        }
		        //Get the marker's location.
		        markerLocation();
		    });
		}

	
		function markerLocation(){
		    //Get location.
		    var currentLocation = marker.getPosition();
		    //Add lat and lng values to a field that we can save.
		    document.getElementById('lat').value = currentLocation.lat(); //latitude
		    document.getElementById('lng').value = currentLocation.lng(); //longitude
		}
	
	</script>

	 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpXVKb63gUuHIPTgTIkXiG-7qON9SI_ms&libraries=places&callback=initAutocomplete"
	 async defer>
	 </script>
</div>


@endsection


