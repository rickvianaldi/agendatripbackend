@extends('master')

@section('title')
Create new tag - {{Auth::User()->name}}
@stop


@section('content')

<div class="container">
<div class="col-sm-6"><h1>Create new Tag
</h1>
</div>
</div>

	<form class="form-horizontal" action="{{url('admin/tagcreate')}}" method="post" autocomplete="off">
			<div class="form-group">
			    <label for="name" class="col-sm-2 control-label">Tag Name</label>
			    <div class="col-sm-6">
			    	{!! Form::text('name',null,['class'=>'form-control'])!!}
			 	 </div>
			 </div>

		

			<div class="form-group">
			    <label for="form" class="col-sm-2 control-label"></label>
			    <div class="col-sm-6">
			    	<input class="btn btn-primary" type="submit" value="Create Tag"></input>
			 	 </div>
			</div>
	</form>



	@if ($errors->any())	
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach

		</ul>
	@endif


@endsection