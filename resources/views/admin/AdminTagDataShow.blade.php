@extends('master')

@section('title')
Tag {{$tag['name']}} - {{Auth::User()->name}}
@stop


@section('content')

			{!! Form::model($tag,['class'=>'form-horizontal','method'=>'POST','autocomplete'=>'off'])!!}
			<div class="form-group">
			    <label for="userid" class="col-sm-2 control-label">Tag ID</label>
			    <label for="userid" class="col-sm-6 control-label" style="text-align:left">{{$tag->id}}</label>
			 </div>

			<div class="form-group">
			    <label for="name" class="col-sm-2 control-label">Tag Name</label>
			    <div class="col-sm-6">
			    	{!! Form::text('name',null,['class'=>'form-control'])!!}
			 	 </div>
			</div>

			<div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label"></label>
			    <div class="col-sm-6">
			    	<input class="btn btn-primary" type="submit" value="Update Tag"></input>
			     </div>
			</div>

			{!! Form::close()!!}

			
	@if ($errors->any())	
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach

		</ul>
	@endif




@endsection