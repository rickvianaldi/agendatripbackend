<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
	<!-- all local references for quicker loading-->
	<!-- <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>  -->

	<script src="{{url()}}/js/vendor/jquery-1.11.2.min.js"></script>
	<script src="{{url()}}/js/vendor/bootstrap.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet"> <!-- bootstrap css included here -->

	<link href="{{ url('/css/adminpanel.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/agendatrip.css') }}" rel="stylesheet">
	
	


	<!-- for specific pages -->
	@if(isset($css))
		@foreach($css as $row)
			<link rel="stylesheet" href="{{url()}}/css/{{$row}}.css">
		@endforeach
	@endif
	
		
	@if(isset($js))
		@foreach($js as $row)
			<script src="{{url()}}/js/{{$row}}.js"></script>
		@endforeach
	@endif
	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="admin-body-logged">
	@include('admin.AdminMenuBar')
	
	<div class="content-container">
	@yield('content')
	</div>

	<footer>
		<div class="splash-image-footer"></div>
	</footer>
	<!-- Scripts -->

	<script> 
		//preventing double submission in every form
		$("form").submit(function() {
		    $(this).submit(function() {
		        return false;
		    });
		    return true;
		});
	</script>		

	@if(isset($js_bottom))
		@foreach($js_bottom as $row)
			<script src="{{url()}}/js/{{$row}}.js"></script>
		@endforeach
	@endif
</body>

</html>
