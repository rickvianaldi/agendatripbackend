@extends('master')

@section('title')
vacationlist response
@stop


@section('content')

<style>
	.tagwrap{
		border:1px solid #EAB11B;
		border-radius:3px;
		background-color:#ffca40;
		padding:6px;
		margin:10px;
		font-size:0.8em;
	}


	.labelchoice:hover {
	    background-color: #A77E13;
	    cursor: pointer;
	}

	.labelchoice {
	    border: 1px solid #EAB11B;
	    border-radius: 3px;
	    background-color: #ffca40;
	    padding: 6px;
	    margin: 11px;
	    font-size: 0.8em;
	    width: 77%;
	    height: 105px;
	}
</style>

<div class="container">
	<h3> Terima kasih atas waktu anda.</h3>
	<div class="bg-success" style="padding:10px;">Response anda telah berhasil kami terima, 
	<br>bantuan anda sangat berarti bagi penelitian kami.
	<br>Have a nice day!!</div>
</div>
@endsection