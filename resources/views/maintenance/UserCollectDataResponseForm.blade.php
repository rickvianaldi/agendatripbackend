@extends('master')

@section('title')
vacationlist response
@stop


@section('content')

<style>
	.tagwrap{
		border:1px solid #EAB11B;
		border-radius:3px;
		background-color:#ffca40;
		padding:6px;
		margin:10px;
		font-size:0.8em;
	}


	.labelchoice:hover {
	    background-color: #A77E13;
	    cursor: pointer;
	}

	.labelchoice {
	    border: 1px solid #EAB11B;
	    border-radius: 3px;
	    background-color: #ffca40;
	    padding: 6px;
	    margin: 11px;
	    font-size: 0.8em;
	    width: 77%;
	    height: 105px;
	}
</style>

<div class="container">
	<h3 class="text-center">Response Form</h3>
	<h3>User ID: {{$user->id}}</h3>
	<h3>Response Name: {{$user->username}}</h3><br>
	<div class="row">
			<div class="col-md-3"></div>
			<div class="col-xs-12 col-md-6 text-justify">
			Halo, Terima kasih atas waktunya bersedia untuk mengisi response form ini,
			<br><br>Form ini dibuat dalam rangka mengumpulkan data preferensi tempat wisata sesuai selera pribadi, 
			bertujuan untuk menyelesaikan skripsi S1 tim Agendatrip Binus University.
			<br><br>Silahkan mengisi jawaban antara "YES" atau "NO" berdasarkan ketertarikan anda untuk mengunjungi tempat wisata yang ada di bawah ini
			<br><br>Jika anda sudah selesai mengisi semua pilihan, silahkan tekan tombol "Submit Response" dibawah
			<br><br><strong><i>Jika anda menggunakan smartphone, silahkan scroll ke samping untuk menemukan tombol yes dan no =></i></strong>
			
			<Br><Br>
			<div class="bg-success" style="padding:10px;">Form pada awalnya akan ada pada posisi "NO"</div >
			</div>
			<div class="col-md-3"></div>
	</div>
	<br><br>

	<!-- form target for submiting response -->
	<form action="{{url('collectdata/submitresponse')}}" method="post">
	<table class="table table-bordered table-striped">
		<tr>			
			<td>Rating</td>
			<td>Nama</td>
			<td>Description</td>
			<td width="220px">Tags</td>
			<td>Image</td>
			<td width="110px">yes</td>	
			<td width="110px">no</td>
		</tr>


		@foreach ($vacationlist as $vacationsite)
			<tr> 
					
				
				<td><p style="color : #ffca40"><strong>{{$vacationsite->rating}}</strong></p></td>
				<td><strong>{{$vacationsite->name}}</strong></td>
				
				<td>{{$vacationsite->address}}</td>
				<td>
					@foreach($tags[$vacationsite->id] as $tag)
			    	<div class="tagwrap">{{$tag->tag->name}}</div>
			    	@endforeach
			    	</td>
				
				<td>
					@if($vacationsite->image)
						<a href="{{url()}}/vacationimages/{{$vacationsite->image}}" data-lightbox="{{$vacationsite->id}}" title="{{$vacationsite->name}}">
							<img style="width:100px;height:100px;"src="{{url()}}/small_vs_thumbs/ts_{{$vacationsite->image}}"></img>
						</a>
					@else
						<div style="width:100px;height:100px;display:table-cell;vertical-align:middle;"><p class="text-center">no<br>photo</p></div>
					@endif
				</td>

				<td>
					<div>
					  <label class="labelchoice" for="radyes{{$vacationsite->id}}">
					  		<input type="radio" id="radyes{{$vacationsite->id}}" name="{{$vacationsite->id}}" value="1"/>
					  		<span>Yes</span>
					  </label>
					</div>

				</td>
				
				<td>	
					<div>
					 <label class="labelchoice" for="radno{{$vacationsite->id}}">
					  <input type="radio" id="radno{{$vacationsite->id}}" name="{{$vacationsite->id}}" value="0" checked="checked" />
					  No
					  </label>
					</div>

				</td>
			</tr>
		@endforeach
		<tr>
			<td colspan="7"><input type="submit" class="btn btn-danger pull-right" value="Submit response"></td>
		</tr>
	</table>
	<input type="hidden" class="btn btn-danger" name="respondenid" value="{{$user->id}}">
	
	</form>
	
	<Br><Br><br><br><Br><Br>



	<!-- prompt delete div -->
			<div class="modal fade bs-delete-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				
			  <div class="modal-dialog modal-sm">										
			    	<div class="modal-content">
				      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Prompt Delete</h4>
				      </div>
					      <div class="modal-body">
					      	
					        <p>Are you sure want to delete this? <div id="vacationname" class="bg-warning text-center"></div><br> This action cannot be undone!!</p>
					      </div>
				      <div class="modal-footer">
					        

					        <form action="{{url()}}/admin/vacationdelete" method="post">
					        	<input id="vacationidtodelete" type="hidden" name="id">
					        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					        	<input type="submit" class="btn btn-danger" value="Delete">
					        </form>
				      </div>
				    </div><!-- /.modal-content -->
			  </div>
			</div>
</div>


<!-- prompt delete script -->
	<script language="javascript" type="text/javascript">
				$('.bs-delete-modal-sm').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal
				  var vacationid = button.data('deleteid')
				  var vacationname = button.data('deleteemail') // Extract info from data-* attributes
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  var modal = $(this)
				  modal.find('.modal-title').text('Delete vacation: ' + vacationname)
				  // document.getElementById("deleteidhere").setAttribute("href","{{url()}}/admin/vacation/delete/"+recipient) 
				  document.getElementById("vacationidtodelete").value = vacationid
				  document.getElementById("vacationname").innerHTML = vacationname
				})		 
	   </script>
@endsection