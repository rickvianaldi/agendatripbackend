@extends('master')

@section('title')
Userlist -
@stop


@section('content')
<div class="container">
		<h3 class="text-center">Responden Panel</h3><br>
		<h4 class="text-center">Terima kasih atas kerjasamanya</h4>
	<a href="{{url('admin/collectdata/create')}}"><button class="btn btn-primary"> + Create Respondent</button></a>
	<br><br>

	<table class="table table-bordered table-striped">
		<tr>
			<td>action</td>
			<td>ID</td>
			<td>FULLNAME</td>
			<td>Response</td>
			<td>Result</td>
		</tr>
		@foreach ($userlist as $user)
			<tr> 
				<td width="150px">
						<td>{{$user->id}}</td>
				<td>{{$user->username}}</td>
				<td width="120px"><a href="{{url('collectdata/response')}}/{{$user->id}}"><button class="btn btn-primary">Go to Response</button></a></td>
				<td width="120px"><a href="{{url('collectdata/result')}}/{{$user->id}}"><button class="btn btn-primary">Result</button></a></td>
			</tr>
		@endforeach
	</table>

			<!-- prompt delete div -->
			<div class="modal fade bs-delete-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				
			  <div class="modal-dialog modal-sm">										
			    	<div class="modal-content">
				      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Prompt Delete</h4>
				      </div>
					      <div class="modal-body">
					      	
					        <p>Are you sure want to delete this user? <div id="username" class="bg-warning text-center"></div><br> This action cannot be undone!!</p>
					      </div>
				      <div class="modal-footer">
					        
<!-- 
					        <form action="{{url()}}/admin/collectdata/userdelete" method="post">
					        	<input id="useridtodelete" type="hidden" name="id">
					        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					        	<input type="submit" class="btn btn-danger" value="Delete">
					        </form> -->
				      </div>
				    </div><!-- /.modal-content -->
			  </div>
			</div>
</div>
		<!-- prompt delete script -->
		<script language="javascript" type="text/javascript">
				$('.bs-delete-modal-sm').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal
				  var userid = button.data('deleteid')
				  var username = button.data('deleteemail') // Extract info from data-* attributes
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  var modal = $(this)
				  modal.find('.modal-title').text('Delete user: ' + username)
				  // document.getElementById("deleteidhere").setAttribute("href","{{url()}}/admin/user/delete/"+recipient) 
				  document.getElementById("useridtodelete").value = userid
				  document.getElementById("username").innerHTML = username
				})		 
	   </script>
	

@endsection