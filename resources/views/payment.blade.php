<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>@yield('title')</title>
		<!-- all local references for quicker loading-->
		<!-- <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>  -->
		<script src="http://localhost/agendatripbackend/public/js/vendor/jquery-1.11.2.min.js"></script>
		<script src="http://localhost/agendatripbackend/public/js/vendor/bootstrap.min.js"></script>
		<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
		<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

		<link href="{{ url('/css/adminpanel.css') }}" rel="stylesheet">
		<link href="{{ asset('/css/agendatrip.css') }}" rel="stylesheet">
	</head>
	<body class ="admin-body">
		<div class="row">
			<img class="logo center-block" src="{{url('/assets/agenda-trip-logo-noshadow.png')}}"></img>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="inner-panel">
						<div class="panel-body text-center">
							@yield('content')
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>
	<footer>
		<div class="splash-image"></div>
	</footer>

	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</html>
