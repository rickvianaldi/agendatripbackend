$( document ).ready(function() {
	//datepicker ui

	    $( ".datepicker" ).datepicker({
		  dateFormat: "yy-mm-dd",
		});


	//disable input scrolling
		$(':input[type=number]').on('mousewheel', function(e){
	    $(this).blur(); 
		});

    //touchspin
    $("input[name='quota']").TouchSpin({
                min: 0,
                max: 1000000,
                step: 1,
                decimals: 0,
            });


    //tiny mce initiate
    tinymce.init({ 
		selector:'textarea',
		valid_elements : 'strong/b,br,ul,li',
		invalid_elements :'p',
		menubar:false,
		toolbar:'bullist bold',
		statusbar:false,
		forced_root_block : false,
		});


	//tag it initiate
	$("#myTags").tagit({
		allowSpaces:true,
		caseSensitive : false,
		 singleField: true,
         singleFieldNode: $('#tagField'),
           preprocessTag: function (val) {
		        if (!val) {
		            return '';
		        }
		        var values = val.split(",");
		        if (values.length > 1) {
		            for (var i = 0; i < values.length; i++) {
		                $("#myTags").tagit("createTag", values[i]);
		            }
		            return ''
		        } else {
		            return val
		        }
				 }
	});

});
	
	//auto change date value
	var firstchange = false;	
	function changedate(val) {
		
	    if(firstchange == false){
	    $('#date_end').val(val) ;
	    firstchange = true;
		}
	}
					

