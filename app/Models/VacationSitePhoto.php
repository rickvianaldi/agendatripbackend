<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacationSitePhoto extends Model {

	protected $table = 'vacation_site_photos';

	public function vacation_site(){
		return $this->belongsTo('App\Models\VacationSite');
	}
	
}
