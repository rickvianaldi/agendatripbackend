<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model {

	public $timestamps = false; //$timestamps wajib diganti false apabila tabel tidak memiliki timestamp default

	protected $table = 'agendas';
	protected $fillable = ['user_id','name','start','end'];

	// relations

	public function user(){
		return $this->belongsTo('App\Models\User');
	}

	public function agenda_details(){
		return $this->hasMany('App\Models\AgendaDetail');
	}

	public function bookings(){
		return $this->hasMany('App\Models\UserBooking');
	}
}
