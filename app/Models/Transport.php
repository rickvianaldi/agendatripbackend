<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model {

	public $timestamps = false; //timestamps wajib diganti false apabila tabel tidak memiliki timestamp

	protected $table = 'transports';
	protected $fillable = ['name'];


}
