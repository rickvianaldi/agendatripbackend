<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserIdentity extends Model {

	//
	protected $table = 'user_identities';
	protected $guarded = ['id'];

	public static $create_params = [
		'title'=>'required',
		'first_name' => 'required',
		'last_name' => 'required',
		'birthdate' => array('required','regex:/\d{4}-\d{2}-\d{2}/'),
		'phone_number' => array('required','numeric'),
	];

	public static $edit_params = [
		'user_identity_id' => array('required','numeric'),
		'title'=>'required',
		'first_name' => 'required',
		'last_name' => 'required',
		'birthdate' => array('required','regex:/\d{4}-\d{2}-\d{2}/'),
		'phone_number' => array('required','numeric'),
	];

	public static $delete_params = [
		'user_identity_id' => array('required', 'numeric'),
	];

	public function user(){
		return $this->belongsTo('App\Models\User');
	}

}
