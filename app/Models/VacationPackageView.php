<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacationPackageView extends Model {
	protected $table = 'vacation_package_views';
	protected $guarded = array('id');


	public function vacation_package(){
		return $this->belongsTo('App\Models\VacationPackage');
	}
}
