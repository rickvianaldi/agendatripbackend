<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCollectDataResponse extends Model {

	public $timestamps = false; //timestamps wajib diganti false apabila tabel tidak memiliki timestamp

	protected $table = 'user_collect_data_response';
	protected $fillable = ['id','vacation_site_id','like'];

}
