<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportDetail extends Model {

	public $timestamps = false; //timestamps wajib diganti false apabila tabel tidak memiliki timestamp

	public static $create_params = [
		'source_site_id' => 'required|numeric',
		'destination_site_id' => 'required|numeric',
		'transport_id' => 'required|numeric',
		'estimated_time' => 'required|numeric',
		'estimated_expense' => 'numeric'
	];

	public static $update_params = [
		'source_site_id' => 'required|numeric',
		'destination_site_id' => 'required|numeric',
		'transport_id' => 'required|numeric',
		'estimated_time' => 'required|numeric',
		'estimated_expense' => 'numeric'
	];

	public $primaryKey ='source_site_id';
	protected $table = 'transport_details';
	protected $fillable = ['user_id','source_site_id','destination_site_id','transport_id','estimated_time','estimated_expense'];

	public function agenda_detail(){
		return $this->belongsTo('App\Models\AgendaDetail','source_site_id','id');
	}

	public function transport(){
		return $this->belongsTo('App\Models\Transport');
	}

}
