<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacationSite extends Model {

	public static $search_by_location_params = [
	'latitude' => 'required | numeric',
	'longitude' => 'required | numeric'];

	public $timestamps = false;

	protected $table = 'vacation_sites';
	protected $fillable = ['name','address','contactnumber','image','description','longitude','latitude'];

	public function photos(){
		return $this->hasMany('App\Models\VacationSitePhoto');
	}

	public function tags(){
		return $this->hasMany('App\Models\VacationSiteTag');
	}
}
