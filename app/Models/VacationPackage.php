<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacationPackage extends Model {

	protected $table = 'vacation_packages';
	protected $guarded = array('id');
	//protected $fillable = ['id','price','quota','title','header','sale_start','sale_end','facility_inclusion'];

	public function tags(){
		return $this->hasMany('App\Models\VacationPackageTag');
	}

	public function views(){
		return $this->hasMany('App\Models\VacationPackageViews');
	}
}
