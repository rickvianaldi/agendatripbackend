<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Auth\Authenticatable;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract{

	use Authenticatable;
	use SoftDeletes;

	public static $create_params = [
		'email' => 'required|email|unique:users',
		'password' => 'required|min:8',
		'firstname' => 'required|min:3',
		'lastname' => 'required|min:3',
	];

	protected $table = 'users';
	protected $guarded = ['id'];
	protected $hidden = ['password'];
	protected $fillable = ['password','firstname','lastname','email']; //details later
  protected $dates = ['deleted_at'];


	//relations

	public function agendas(){
		return $this->hasMany('App\Models\Agenda');
	}

	public function wishes(){
		return $this->hasMany('App\Models\UserWish');
	}

	public function vacation_package_purchases(){
		return $this->hasMany('App\Models\VacationPackagePurchase');
	}

	public function preferences(){
		return $this->hasMany('App\Models\UserTag');
	}

	public function bookings(){
		return $this->hasMany('App\Models\UserBooking');	
	}

	public function identities(){
		return $this->hasMany('App\Models\UserIdentity');
	}

	//functions

	public function create_app_key(){
		$token = bin2hex(str_random(16));
		$this->app_key = $token;
		if($this->save()){
			return $token;
		}
		else{
			return null ;
		}
	}

	public function destroy_app_key(){
		$this->app_key = null;
		$this->save();
	}
}
