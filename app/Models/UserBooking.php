<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBooking extends Model {
	//since we gonna ask for tiket.com for the status of user's bookings ( we can't know whether user has paid , we will ask tiket.com everytime user asks the stats yay)
	public $timestamps = true;
	//
	public $order;
	protected $guarded = ['id'];


	public function toArray()
    {
        $array = parent::toArray();
        $array['order'] = $this->order;
        return $array;
    }
/*
	public function getStatus(){
		return $this->quantity;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function getOrder(){
		return $this->order;
	}

	public function setOrder($order){
		$this->order = $order;
	} 

*/
	public function user(){
		return $this->belongsTo('App\Models\User');
	}

	public function agenda(){
		return $this->belongsTo('App\Models\Agenda');
	}
	
}
