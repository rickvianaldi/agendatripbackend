<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTag extends Model {

	public $timestamps = false; //timestamps wajib diganti false apabila tabel tidak memiliki timestamp
	protected $table = 'user_tags';
	protected $fillable = ['user_id','tag_id','value'];

	//
	public function user(){
		return $this->belongsTo('App\Models\User');
	}
}
