<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCollectData extends Model {

	public $timestamps = false; //timestamps wajib diganti false apabila tabel tidak memiliki timestamp

	protected $table = 'user_collect_data';
	protected $fillable = ['id','username'];

}
