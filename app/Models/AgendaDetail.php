<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgendaDetail extends Model {

	public static $create_params = [
		'agenda_id' => 'required|numeric',
		'vacation_site_id' => 'required|numeric',
		'start' => 'required|date',
		'end' => 'required|date',
	];

	public static $update_params = [
		'agenda_detail_id' => 'required|numeric',
		'vacation_site_id' => 'required|numeric',
		'start' => 'required|date',
		'end' => 'required|date',
	];

	public $timestamps = false; //timestamps wajib diganti false apabila tabel tidak memiliki timestamp

	protected $table = 'agenda_details';
	protected $fillable = ['user_id','vacation_site_id','start','end'];

	public function agenda(){
		return $this->belongsTo('App\Models\Agenda');
	}

	public function vacation_site(){
		return $this->belongsTo('App\Models\VacationSite');
	}

	public function transport_detail(){
		return $this->hasOne('App\Models\TransportDetail','source_site_id','id');
	}
	
}
