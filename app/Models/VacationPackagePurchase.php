<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacationPackagePurchase extends Model {

	//
	protected $table = 'vacation_package_purchases';
	protected $guarded = array('id');

	//
	public function user(){
		return $this->belongsTo('App\Models\User');
	}

	public function vacation_package(){
		return $this->belongsTo('App\Models\VacationPackage');
	}
}
