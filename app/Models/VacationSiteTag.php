<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class VacationSiteTag extends Model {

	public $timestamps = false; //timestamps wajib diganti false apabila tabel tidak memiliki timestamp default

	protected $table = 'vacation_site_tags';
	protected $fillable = ['tag_id','vacation_site_id','value'];

	public function tag(){
		return $this->belongsTo("App\Models\Tag");
	}

	public function vacation_site(){
		return $this->belongsTo("App\Models\VacationSite");
	}
}
