<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWish extends Model {

	public $timestamps = true; //timestamps wajib diganti false apabila tabel tidak memiliki timestamp

	protected $table = 'user_wishes';
	protected $fillable = ['user_id','vacation_site_id'];

	public function user(){
		return $this->belongsTo('App\Models\User');
	}

	public function vacation_site(){
		return $this->belongsTo('App\Models\VacationSite');
	}
	
}
