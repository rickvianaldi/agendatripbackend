<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Auth\Guard;
class UserAuthServiceProvider extends ServiceProvider {	

	public function register(){
	    Auth::extend('userEloquent', function($app){
	       // you can use Config::get() to retrieve the model class name from config file
	       $myProvider = new EloquentUserProvider($app['hash'], '\App\Models\User');
	      	return new Guard($myProvider, $app['session.store']);
	    });
	    $this->app->singleton('auth.driver_user', function($app){
	       	return Auth::driver('userEloquent');
	    });
	}
}