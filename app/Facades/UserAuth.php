<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class UserAuth extends Facade {
        protected static function getFacadeAccessor() { return 'auth.driver_user'; }
    }