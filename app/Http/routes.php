<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



//note : /home not defined yet | homeless :<


Route::get('/', 'WelcomeController@index');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes... 
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//grouping route untuk admin pakai prefixes
Route::group(['prefix'=>'admin','middleware' => 'auth'], function()
{


    Route::get('/', 'Admin\AdminHomeController@index');
    Route::get('/home', 'Admin\AdminHomeController@index');

    //User management Routes
        //list
   
        Route::get('/user/','Admin\AdminUserListController@getUserList');
        Route::get('/user/{id}','Admin\AdminUserListController@getUserData');
        //update
        Route::POST('/user/{id}','Admin\AdminUserListController@updateUser'); //Update current user (this has different validation with create)
        //create
        Route::get('/usercreate','Admin\AdminUserListController@createUser');
        Route::post('/usercreate','Admin\AdminUserListController@storeUser');
        //delete
        Route::post('/userdelete','Admin\AdminUserListController@deleteUser');

        //Route 
        Route::get('/userbooking/{id}','Admin\AdminUserListController@getUserBookingList');

  	//Booking routes
        Route::get('/bookings','Admin\AdminBookingController@getBookinglist');


    //Vacation Site management routes
        //list
	   
	    Route::get('/vacationsite','Admin\AdminVacationSiteController@getVacationSiteList'); //list
	    //create
	    Route::get('/vacationsite/create','Admin\AdminVacationSiteController@createVacationSite'); //Create - this only redirect to view
	    Route::POST('/vacationsite/create','Admin\AdminVacationSiteController@storeVacationSite');

	    //upload dropzone.js ajax
	    Route::post('/dropzone/uploadFiles/{fileprefix}', 'DropzoneController@uploadFiles');

	    //edit
	    Route::POST('/vacationsite/deletesingleimage','Admin\AdminVacationSiteController@deleteSingleVacationSiteImage'); //deleteimage

	    Route::get('/vacationsite/edit/{id}','Admin\AdminVacationSiteController@editVacationSite'); //edit
	    Route::POST('/vacationsite/edit/{id}','Admin\AdminVacationSiteController@updateVacationSite'); //store edited data

	    //delete
	    Route::POST('/vacationdelete','Admin\AdminVacationSiteController@deleteVacationSite'); //delete reference to posted vacation id
	    //details
	    Route::get('/vacationsite/{id}','Admin\AdminVacationSiteController@getVacationSite'); //details - must be bottom because of wildcard use

	//tags management routes
	    Route::get('/tag','Admin\AdminTagsController@getTags');
	    Route::get('/tag/edit/{id}','Admin\AdminTagsController@getTag'); //edit
	    Route::POST('/tag/edit/{id}','Admin\AdminTagsController@updateTag'); //edit

	    Route::get('/tagcreate','Admin\AdminTagsController@createTag');
	    Route::post('/tagcreate','Admin\AdminTagsController@storeTag');
	    Route::post('/tagdelete','Admin\AdminTagsController@deleteTag');

	    Route::get('/profile', function()
	    {
	       return('this is profile');
	    });


	//Vacation Package Routes
	    //listing
	
	    Route::get('/vacationpackage','Admin\AdminVacationPackageController@getVacationPackageList');

	    //create
	    Route::get('/vacationpackage/create','Admin\AdminVacationPackageController@createVacationPackage'); //Create - this only redirect to view
	    Route::POST('/vacationpackage/create','Admin\AdminVacationPackageController@storeVacationPackage');

	    //upload dropzone.js ajax
	    Route::post('/dropzone/UploadVacationPackageImages/{fileprefix}', 'DropzoneController@UploadVacationPackageImages');
	    //edit
	    Route::get('/vacationpackage/edit/{id}','Admin\AdminVacationPackageController@editVacationPackage'); //edit
	    Route::POST('/vacationpackage/edit/{id}','Admin\AdminVacationPackageController@updateVacationPackage'); //store edited data
	
	    //delete
	    Route::POST('/vacationpackagedelete','Admin\AdminVacationPackageController@deleteVacationPackage'); //delete reference to posted vacation id

	    //details
	    Route::get('/vacationpackage/{id}','Admin\AdminVacationPackageController@getVacationPackage'); //details - must be bottom because of wildcard use





	//Agenda Routes

	    
	    Route::POST('/agendadelete','Admin\AdminAgendaController@deleteAgenda');
	    Route::get('/agenda/{id}','Admin\AdminAgendaController@userAgenda'); //displaying user agenda by id
	//Agenda Detail Routes
	    Route::get('/agendadetail/{id}','Admin\AdminAgendaController@agendaDetails');

	
//maintenance routes
	    Route::get('/generatemoderatethumbs','Admin\generatemoderatethumbs@index'); //check?
	    Route::get('/generatevacationsiteseed','Admin\AdminMaintenanceController@GenerateVacationSiteSeed');
	    Route::get('/generatetagseed','Admin\AdminMaintenanceController@GenerateTagSeed');
	    Route::get('/generatevacationsitephotoseed','Admin\AdminMaintenanceController@generateVacationSitePhotoSeed');
	    Route::get('/generatevacationsitetagseed','Admin\AdminMaintenanceController@generateVacationSiteTagSeed');
	    Route::get('/collectdata/create',function(){return view('maintenance.UserCollectDataCreate');});

	    //vacation packages
	    Route::get('/generatevacationpackage','Admin\AdminMaintenanceController@generateVacationPackage');
	    Route::get('/generatevacationpackagetag','Admin\AdminMaintenanceController@generateVacationPackageTag');

}); //end of route group admin


 //collecting data
	    Route::get('/collectdata','Admin\AdminMaintenanceController@usercollectdata');
	    
	    Route::POST('/collectdata/userstore','Admin\AdminMaintenanceController@usercollectdatastore');

	    //tag values
	    Route::get('/tagvalues','Admin\AdminMaintenanceController@tagvalues');

	    	//delete user collect data
	    // Route::POST('/collectdata/userdelete','Admin\AdminMaintenanceController@usercollectdatadelete');

	    Route::get('/collectdata/response/{id}','Admin\AdminMaintenanceController@usercollectdataresponseform');
	    Route::get('/collectdata/result/{id}','Admin\AdminMaintenanceController@usercollectdataresult');
	    Route::POST('/collectdata/submitresponse','Admin\AdminMaintenanceController@usercollectdatastoreresponse'); //form target for submitting respose

	    //Route::get('/randomizetag','Admin\AdminMaintenanceController@randomvalue');



Route::group(['namespace'=>'api','prefix'=>'/payment'],function(){
	Route::get('/pay','PaymentController@pay');
	Route::get('/success','PaymentController@success');
	Route::get('/unfinished','PaymentController@unfinished');
	Route::get('/error','PaymentController@error');
	Route::post('/callback','PaymentController@callback');
});

Route::group(['namespace' => 'api', 'prefix' =>'api/v1'],function(){
	Route::resource('user','UserController',
		array('only' => array('create','store','edit','update')));

	Route::group(['middleware' => 'user.api'],function(){
		Route::group(['prefix'=>'user/wish'],function(){
		  Route::get('/','UserWishController@index');
		  Route::post('/','UserWishController@create');
		  Route::post('/delete','UserWishController@destroy');
		  Route::post('/delete_by_vs','UserWishController@destroy_by_site');
		});
		Route::group(['prefix'=>'user/identity'],function(){
		  Route::get('/','UserIdentityController@index');
		  Route::post('/','UserIdentityController@create');
		  Route::post('/edit','UserIdentityController@edit');
		  Route::post('/delete','UserIdentityController@delete');
		});
	    Route::group(['prefix' => 'tags'],function(){
	      Route::get('/get',['uses'=>'UserTagController@showtags']);
	      Route::post('/new','UserTagController@create');
	      Route::post('/vp_response','UserTagController@modifyTagByPackage');
	      Route::post('/vs_response','UserTagController@modifyTagBySite');
	    });
		Route::group(['prefix' => 'agenda'],function(){
			Route::group(['prefix' => 'detail'],function(){
				Route::group(['prefix' => 'transport'],function(){
					Route::post('/create','TransportDetailController@create');
					Route::post('/update','TransportDetailController@update');
					Route::post('/delete','TransportDetailController@destroy');
				});
				Route::get('/','AgendaDetailController@index');
				Route::get('/show','AgendaDetailController@show');
				Route::post('/update','AgendaDetailController@update');
				Route::post('/create','AgendaDetailController@create');
				Route::post('/delete','AgendaDetailController@destroy');
			});
			Route::get('/','AgendaController@index');
			Route::get('/show','AgendaController@show');
			Route::post('/update','AgendaController@update');
			Route::post('/create','AgendaController@create');
			Route::post('/delete','AgendaController@destroy');
		});
    });


    Route::group(['prefix' => 'bookings'],function(){
      Route::get('/getToken','FlightBookingController@getToken');
      Route::get('/getAirports','FlightBookingController@getAirports');
      Route::get('/getFlights','FlightBookingController@getFlights');
      Route::get('/getCountries','FlightBookingController@getCountries');
      Route::get('/getFlightData','FlightBookingController@getFlightData');
	  Route::get('/addOrder',['uses'=>'FlightBookingController@addOrder','middleware'=>'user.record']);

      Route::group(['prefix'=>'/hotels'],function(){
      	Route::get('/autoComplete','HotelBookingController@autoComplete');
      	Route::get('/searchHotel','HotelBookingController@searchHotel');
      	Route::get('/addOrder',['uses'=>'HotelBookingController@addOrder','middleware'=>'user.record']);
      });

      Route::group(['prefix'=> '/trains'],function(){
	      Route::get('/getStations','TrainBookingController@getStations');
	      Route::get('/getTrains','TrainBookingController@getTrains');
	      Route::get('/addOrder',['uses'=>'TrainBookingController@addOrder','middleware'=>'user.record']);
      });
      Route::get('/showOrders',['uses'=>'UserBookingController@index','middleware'=>'user.api']);
      Route::get('showAgendaOrders',['uses'=>'UserBookingController@showAgendaBooking','middleware'=>'user.api']);
      Route::get('/showOrderDetail',['uses'=>'UserBookingController@show','middleware'=>'user.api']);
    });

	Route::group(['prefix' => 'destination'],function(){
		Route::post('/show','VacationSiteController@show');
		Route::post('/keyword',['uses'=>'SearchController@searchByKey','middleware'=>'user.record']);
		Route::post('/top',['uses'=>'SearchController@SearchByTop','middleware'=>'user.record']);
		Route::post('/nearby',['uses'=>'SearchController@SearchByPosition','middleware'=>'user.record']);
    Route::post('/recommended',['uses'=>'SearchController@searchByPreference','middleware'=>'user.api']);
	});

	Route::group(['prefix' => 'deals'],function(){
		Route::post('/',['uses'=>'VacationPackageController@fresh','middleware' => 'user.record']);
		Route::post('/hot',['uses'=>'VacationPackageController@hot','middleware'=> 'user.record']);
		Route::post('/show',['middleware'=>'user.record','uses'=>'VacationPackageController@show']);
    	Route::post('/recommended',['uses'=>'VacationPackageController@searchByPreference','middleware'=>'user.api']);
    	Route::post('/search',['uses'=>'VacationPackageController@searchByName','middleware'=>'user.record']);
	    Route::group(['prefix' => 'purchases','middleware' => 'user.api'],function(){
	        Route::get('/','VacationPackagePurchaseController@index');
	        Route::post('/new','VacationPackagePurchaseController@create');
	        Route::get('/show','VacationPackagePurchaseController@show');
	    });
	});

	Route::post('login','SessionController@create');
	Route::post('logout','SessionController@destroy');

	Route::get('check','SessionController@validates');
});
