<?php namespace App\Http\Controllers\api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TransportDetail;
use Illuminate\Http\Request;
use Session;

class TransportDetailController extends Controller {

	public function create(Request $request)
	{
		$u = Session::get('user');
		$agenda_id = $request['agenda_id'];
		$agenda = $u->agendas()->find($agenda_id);
		if($agenda != null){
			$agenda_detail_id = $request['source_site_id'];
			$agenda_detail = $agenda->agenda_details()->find($agenda_detail_id);
			if($agenda_detail != null){
				$this->validate($request,TransportDetail::$create_params);
				$input = $request->only('destination_site_id','transport_id','estimated_expense','estimated_time');
				$input['user_id'] = $u->id;
				$transport_detail = $agenda_detail->transport_detail()->create($input);
				return response()->json(['status' => 'ok', 'message' => '', 'payload' => [] ], 200);		
			}
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}

	public function show(Request $request)
	{
		$u = Session::get('user');
		$agenda_id = $request['agenda_id'];
		$agenda = $u->agendas()->find($agenda_id);
		if($agenda != null){
			$agenda_detail_id = $request['agenda_detail_id'];
			$agenda_detail = $agenda->agenda_details()->find($agenda_detail_id);
			if($agenda_detail != null){
				$transport_detail_id = $request['transport_detail_id'];
				$transport_detail = $agenda_detail->transport_detail()->find($transport_detail_id);
				if($transport_detail !=  null){
					return response()->json(['status' => 'ok', 'message' => '', 'payload' => $transport_detail->toJson() ], 200);		
				}
			}
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);	
	}

	public function update(Request $request)
	{
		$u = Session::get('user');
		$agenda_id = $request['agenda_id'];
		$agenda = $u->agendas()->find($agenda_id);
		if($agenda != null){
			$agenda_detail_id = $request['source_site_id'];
			$agenda_detail = $agenda->agenda_details()->find($agenda_detail_id);
			if($agenda_detail != null){
				$transport_detail = $agenda_detail->transport_detail()->find($agenda_detail_id)->first();
				if($transport_detail !=  null){
					$this->validate($request,TransportDetail::$update_params);
					$inputs = $request->only('source_site_id','destination_id','transport_id','estimated_time','estimated_expense');
					$transport_detail->update($inputs);
					return response()->json(['status' => 'ok','message' => '', 'payload' => [] ],200);
				}
			}
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);	
	}

	public function destroy(Request $request)
	{
		$u = Session::get('user');
		$agenda_id = $request['agenda_id'];
		$agenda = $u->agendas()->find($agenda_id);
		if($agenda != null){
			$agenda_detail_id = $request['agenda_detail_id'];
			$agenda_detail = $agenda->agenda_details()->find($agenda_detail_id);
			if($agenda_detail != null){
				$transport_detail = $agenda_detail->transport_detail()->find($agenda_detail_id);
				if($transport_detail !=  null){
					$transport_detail->delete();
					return response()->json(['status' => 'ok','message' => '', 'payload' => [] ],200);
				}
			}
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);		
	}

}
