<?php namespace App\Http\Controllers\api;

use App\Models\UserTag;
use App\Models\VacationPackage;
use App\Models\VacationPackageTag;
use App\Models\VacationSite;
use App\Models\Tag;

use App\Http\Requests;
use App\Http\Responses;
use App\Http\Controllers\Controller;

use DB;
use Session;
use Illuminate\Http\Request;

class UserTagController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function create(Request $request)
	{
		$tags = $request->only('tags')['tags'];
		$user_id = Session::get('user')->id;
		$arr = [];
		foreach($tags as $tag){
			$usertag = UserTag::firstOrCreate(array('user_id'=>$user_id,'tag_id'=>$tag));
			$usertag->value = $usertag->value <= 0.5 ? 0.5 : $usertag->value;
			$usertag->save();
		}
		return response()->json(['status'=>'ok','message'=>'','payload'=>[]],200);
	}

	public function showtags(){
		$tags = Tag::all();
		return response()->json(['status'=>'ok','message'=>'','payload'=>$tags]);
	}

	public function modifyTagByPackage(Request $request)
	{
			$vacation_package_id = $request->get('vacation_package_id');
			$user_id = Session::get('user')->id;
			$vp = VacationPackage::find($vacation_package_id);
			if($vp != null && ($request['response'] == 1 || $request['response'] == 0)){
				if($vp->tags()->count()>0){
					$hx = DB::Select(DB::raw('select IFNULL((1 / (1 + exp(-1 * SUM(vacation_package_tags.value * user_tags.value))) ),0) as hypotheses FROM vacation_packages LEFT JOIN vacation_package_tags on vacation_packages.id = vacation_package_tags.vacation_package_id LEFT JOIN (SELECT * FROM user_tags WHERE user_id = :user_id) as user_tags ON vacation_package_tags.tag_id = user_tags.tag_id  WHERE vacation_packages.id = :vacation_package_id  GROUP BY vacation_package_tags.vacation_package_id'),array('user_id'=>$user_id,'vacation_package_id'=>$vacation_package_id))[0]->hypotheses;

					$answer = $request['response'];
					$cost = -$answer * log($hx) + (1-$answer) * log(1-$hx);

					$tags = DB::table('vacation_package_tags')->leftJoin(DB::raw('(SELECT * FROM user_tags WHERE  user_id = ? ) as user_tags'),function($join){
						$join->on('user_tags.tag_id','=','vacation_package_tags.tag_id');
					})->setBindings([$user_id])->where(function($query) use ($vacation_package_id){
						$query->where('vacation_package_tags.vacation_package_id','=',$vacation_package_id);
					})->select('vacation_package_tags.tag_id',DB::raw('(vacation_package_tags.value) as vp_value'), DB::raw('(CASE WHEN user_tags.value IS NULL THEN COUNT(user_tags.tag_id) ELSE user_tags.value END) AS user_value'))->groupBy('vacation_package_tags.tag_id')->get();

					$learningrate = 0.01;

					foreach($tags as $tag){
						if($tag->user_value==0)
						$tag->user_value = rand(1,50) / 1000;
						$tag->user_value = $tag->user_value - $learningrate*($hx-$answer)*$tag->vp_value;
						$new_tag = UserTag::firstOrNew(['user_id'=>$user_id,'tag_id'=>$tag->tag_id]);
						$new_tag->value = $tag->user_value;
						$new_tag->save();
					}
				}
						return response()->json(['status'=>'ok','message'=>'','payload'=>[]],200);
			}
			return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}

	public function modifyTagBySite(Request $request){
		$vacation_site_id = $request['vacation_site_id'];
		$user_id = Session::get('user')->id;
		if(VacationSite::find($vacation_site_id) != null  && ($request['response'] == 1 || $request['response'] == 0)){

			$hx = DB::Select(DB::raw('select (1 / (1 + exp(-1 * SUM(vacation_site_tags.value * user_tags.value))) ) as hypotheses FROM vacation_site_tags LEFT JOIN (SELECT * FROM user_tags WHERE user_id = :user_id) as user_tags ON vacation_site_tags.tag_id = user_tags.tag_id WHERE vacation_site_tags.vacation_site_id = :vacation_site_id  GROUP BY vacation_site_tags.vacation_site_id'),array('user_id'=>$user_id,'vacation_site_id'=>$vacation_site_id))[0]->hypotheses;

			$answer = $request['response'];
			$cost = -$answer * log($hx) + (1-$answer) * log(1-$hx);

			$tags = DB::table('vacation_site_tags')->leftJoin(DB::raw('(SELECT * FROM user_tags WHERE  user_id = ? ) as user_tags'),function($join){
				$join->on('user_tags.tag_id','=','vacation_site_tags.tag_id');
			})->setBindings([$user_id])->where(function($query) use ($vacation_site_id){
				$query->where('vacation_site_tags.vacation_site_id','=',$vacation_site_id);
			})->select('vacation_site_tags.tag_id',DB::raw('(vacation_site_tags.value) as vp_value'), DB::raw('(CASE WHEN user_tags.value IS NULL THEN COUNT(user_tags.tag_id) ELSE user_tags.value END) AS user_value'))->groupBy('vacation_site_tags.tag_id')->get();

			$learningrate = 0.01;

			foreach($tags as $tag){
				if($tag->user_value==0)
				$tag->user_value = rand(1,50) / 1000;
				$tag->user_value = $tag->user_value - $learningrate*($hx-$answer)*$tag->vp_value;
				$new_tag = UserTag::firstOrNew(['user_id'=>$user_id,'tag_id'=>$tag->tag_id]);
				$new_tag->value = $tag->user_value;
				$new_tag->save();
			}
					return response()->json(['status'=>'ok','message'=>'','payload'=>[]],200);
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}


}
