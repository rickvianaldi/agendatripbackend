<?php namespace App\Http\Controllers\api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\UserIdentity;

use Illuminate\Http\Request;
use Session;

class UserIdentityController extends Controller {

	public function index(){
		$u = Session::get('user');
		$user_identities = $u->identities()->get();
		return response()->json(['status'=>'OK','message'=>'','payload'=>$user_identities],200);
	}

	public function create(Request $request){
		$this->validate($request,UserIdentity::$create_params);
		$u = Session::get('user');
		$params = $request->only('title','first_name','last_name','birthdate','phone_number','email','id_number','passport_number');
		$params['user_id'] = $u->id;
		$user_identity = UserIdentity::create($params);
		return response()->json(['status'=>'OK','message'=>'Created User Identity','payload'=>''],200);
	}

	public function edit(Request $request){
		$this->validate($request,UserIdentity::$edit_params);
		$u = Session::get('user');
		$user_identity = UserIdentity::find($request->get('user_identity_id'));

		// If the user identity does not belong to the current user or if the id of the identity does not exist then return 401
		if(is_null($user_identity) || $user_identity->user_id != $u->id){
			return return_401();
		}
		// Else update the identity and return success report
		$params = $request->only('title','first_name','last_name','birthdate','phone_number','email','id_number','passport_number');
		$user_identity->update($params);
		return response()->json(['status'=>'OK','message'=>'Updated User Identity','payload'=>''],200);
	}

	public function delete(Request $request){
		$this->validate($request,UserIdentity::$delete_params);
		$u = Session::get('user');
		$user_identity = UserIdentity::find($request->get('user_identity_id'));

		// If the user identity does not belong to the current user or if the id of the identity does not exist then return 401
		if(is_null($user_identity) || $user_identity->user_id != $u->id){
			return return_401();	
		}

		// Else delete the identity and return success report 
		$user_identity->delete();
		return response()->json(['status'=>'OK','message'=>'Deleted User Identity','payload'=>''],200);
	}
}
