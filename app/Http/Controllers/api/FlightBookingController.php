<?php namespace App\Http\Controllers\api;
	
set_time_limit(640);

use App\Models\Agenda;
use App\Models\User;
use App\Models\UserBooking;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use GuzzleHttp\Promise;

use Illuminate\Http\Request;
use Session;

class FlightBookingController extends Controller {

	public function getToken(){
		$client = new \GuzzleHttp\Client();
		$res =$client->request('GET','https://api.sandbox.tiket.com/apiv1/payexpress?method=getToken&secretkey=9297a19ffbcba0951474617e1715722e&output=json',['verify' => false,'SYNCHRONOUS' => true]);
		return response()->json(json_decode($res->getBody()->getContents()),$res->getStatusCode());
	}

	public function getAirports(Request $request){
		$this->validate($request,['token'=>'required']);
		$token = $request->get('token');
		$client = new \GuzzleHttp\Client();
		$url = 'https://api-sandbox.tiket.com/flight_api/all_airport?token='.$token.'&output=json';
		$res = $client->request('GET',$url,['verify' => false,'synchronous' => true]);
		return response()->json(json_decode($res->getBody()->getContents()),$res->getStatusCode());
	}

	public function getFlights(Request $request){
		$this->validate($request,[
			'a'=>'required',
			'd'=>'required',
			'date'=>array('required','regex:/\d{4}-\d{2}-\d{2}/'),
			'ret_date'=>'regex:/\d{4}-\d{2}-\d{2}/',
			'adult'=>array('required','numeric','min:1'),
			'child'=>array('required','numeric','min:0'),
			'infant'=>array('required','numeric','min:0'),
			'token'=>'required',
			'round_trip'
		]);
		$params = $request->only('a','d','date','adult','child','infant','token','round_trip');
		if($params['round_trip']==true||!is_null($params['ret_date'])){
			$params['ret_date']=$request->get('ret_date');
		}
		else if(is_null($params['round_trip'])){
			$params['round_trip']=false;
		}
		$params['output']='json';
		//dd($params);
		$client = new \GuzzleHttp\Client();
		$url = 'https://api-sandbox.tiket.com/search/flight';
		$res = $client->request('GET',$url,['verify'=>false,'synchronous' => true,'query'=>$params]);
		return response()->json(json_decode($res->getBody()->getContents()),$res->getStatusCode());
	}

	public function getCountries(Request $request){
		$this->validate($request,['token' => 'required']);
		$token = $request->get('token');
		$client = new \GuzzleHttp\Client();
		$url = 'https://api-sandbox.tiket.com/general_api/listCountry?token='.$token.'&output=json';
		$res = $client->request('GET',$url,['verify' => false,'synchronous' => true]);
		return response()->json(json_decode($res->getBody()->getContents()),$res->getStatusCode());
	}
	
	public function getFlightData(Request $request){
		$this->validate($request,['token' => 'required'
								,  'flight_id' => 'required'
								,  'date' => 'required']);
		$token = $request->get('token');
		$flight_id = $request->get('flight_id');
		$date = $request->get('date');
		$ret_flight_id = $request->get('ret_flight_id');
		$ret_date = $request->get('ret_date');
		
		$client = new \GuzzleHttp\Client();
		$url = env('TIKET_API').'/flight_api/get_flight_data?flight_id='.$flight_id.'&token='.$token.'&date='.$date.'&output=json';
		if(!is_null($ret_flight_id) && !is_null($ret_date)){
			$url = $url.'&ret_date='.$ret_date.'&ret_flight_id='.$ret_flight_id;
		}
		$res = $client->request('GET',$url,['verify' => false, 'synchronous' => true]);
		return response()->json(json_decode($res->getBody()->getContents()),$res->getStatusCode());
	}

	public function addOrder(Request $request){
		$this->validate($request,['token' => 'required',
								  'flight_id' => 'required',
								   'date' => 'required']);
		$client = new \GuzzleHttp\Client();
		$data['token'] = $request->get('token');
		$params = $request->all();
		$params['output']='json';
		
		
		$u = Session::get('user');
		if($request->get('key') != null)
			$key = $request->get('key');
		else $key = "wrongkey";

		$url = env('TIKET_API').'/order/add/flight';
		$res = null;
		$res_order = null;
		$result_customer_login = null;
		$result_payment_methods = null ;
		//=================================================================
		// STEP 1 : ADD ORDER TO THE API OF TIKET.COM
		//=================================================================
		$res = $client->request('GET',$url,['verify' => false, 'synchronous' =>true, 'query' => $params,'timeout' => 310]);
		$result_json = json_decode($res->getBody()->getContents());
		$result_json->step='step1';
		if(property_exists($result_json->diagnostic,'confirm')){
		//=================================================================
		// IF ABOVE CHECKS WHETHER WE SUCCESSFULLY ORDERED OR NOT. FAILURE MIGHT BE CAUSED BY DOUBLE ORDER( EXACT SAME NAME AND FLIGHT CHOICE IN ORDERING)
		//=================================================================
			$url_order = env('TIKET_API').'/order';
			$param_order['token']=$data['token'];
			$param_order['output']='json';
			$res = $client->request('GET',$url_order,['verify' => false, 'synchronous' => true,'query' => $param_order]);
		//=================================================================
		// STEP 2 : RETRIEVE ORDER ID. THE ADD ORDER API DOESNT RETURN ORDER ID
		//=================================================================
			$result_json = json_decode($res->getBody()->getContents());
			$result_json->step='step2';
			if(property_exists($result_json->diagnostic,'confirm'))
			{	
		//=================================================================
		// CHECKS IF ORDER ACTUALLY EXISTS. AND GET SOME DATA		//=================================================================
				$data['order_id'] = $result_json->myorder->order_id;
				$data['order_detail_id'] = $result_json->myorder->data[0]->order_detail_id;
				$data['order_type'] = $result_json->myorder->data[0]->order_type;
				$data['order_expiration'] = $result_json->myorder->data[0]->order_expire_datetime;
				$data['order_amount'] = intval($result_json->myorder->total_without_tax);
				if(!is_null($u) && strcmp($u->app_key,$key)==0){
					$data['user_id'] = $request->get('userID');	
					if($request->get('agenda_id')!=null){
						$agenda = Agenda::find($request->get('agenda_id'));
						if(!is_null($agenda)){
							if($agenda->user_id == $u->id){
								$data['agenda_id'] = $agenda->id;
							}						
						}
					}
				}
				$data['contact_email'] = $request->get('conEmailAddress');

				$url_checkout_customer = env('TIKET_API').'/checkout/checkout_customer';

				$customer_informations['token'] = $data['token'];
				$customer_informations['salutation'] = $params['conSalutation'];
				$customer_informations['firstName'] = $params['conFirstName'];
				$customer_informations['lastName'] = $params['conLastName'];
				$customer_informations['phone'] = $params['conPhone'];
				$customer_informations['emailAddress'] = $params['conEmailAddress'];
				$customer_informations['output']='json';
				$customer_informations['saveContinue']='2';
		//=================================================================
		// STEP 3 : CHECKS OUT CUSTOMER INFORMATION		//=================================================================
		
				$res = $client->request('GET',$url_checkout_customer,['verify' => false, 'synchronous' => true, 'query' => $customer_informations]);
				$result_json = json_decode($res->getBody()->getContents());
				$result_json->step='step3';	
				if(property_exists($result_json->diagnostic,'confirm')) {
					$url_payment_methods = env('TIKET_API').'/checkout/checkout_payment';
					$data_payment_method['token'] = $data['token'];
					$data_payment_method['output'] = 'json';
					$res = $client->request('GET',$url_payment_methods,['verify'=>false,'synchronous' => true,'query' => $data_payment_method]);
					$result_json = json_decode($res->getBody()->getContents());
					$result_json->step='step4';
					if(property_exists($result_json->diagnostic,'confirm')){
						$url_payment_checkout = env('TIKET_API').'/checkout/checkout_payment/3';
						$data_payment_checkout['token'] = $data['token'];
						$data_payment_checkout['output'] ='json';
						$data_payment_checkout['btn_booking'] = '1';
						$data_payment_checkout['user_bca']='pengguna1212';
						$res = $client->request('GET',$url_payment_checkout,['verify'=>false,'synchronous' =>true,'query'=>$data_payment_checkout]);
						$result_json = json_decode($res->getBody()->getContents());
						$result_json->step='step5';
						if(property_exists($result_json->diagnostic,'confirm')){
							$data['order_amount'] = intval($result_json->grand_total);
							$user_booking = UserBooking::create($data);
							$user_booking->save();
							$result_json->booking_id=$user_booking->id;
							response()->json($result_json);
						}
					}
				}
			}
		}
		return response()->json($result_json);
	}

	

}
