<?php namespace App\Http\Controllers\api;

use App\Models\VacationSite;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class VacationSiteController extends Controller {

	
	public function show(Request $request)
	{
		$vacation_site_id = $request['vacation_site_id'];
		$vacation_site = VacationSite::with('photos','tags','tags.tag')->find($vacation_site_id);
		return response()->json(['status' => 'ok', 'message' => '', 'payload' => $vacation_site ], 200);	
	}

}
