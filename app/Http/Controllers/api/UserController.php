<?php namespace App\Http\Controllers\api;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller {
	/**
	 * Creates new user, returns status of creation
	 */
	public function store(Request $request)
	{
		$this->validate($request,User::$create_params);
		$data = $request->only('email','password','firstname','lastname');
		$data['password'] = bcrypt($data['password']);
		
		if(User::create($data)){
			return response()->json(['status' => 'ok' , 'message' => 'User created'],201);
		}
		return response()->json(['status' => 'error', 'message' => 'Registration error !'],500);
	}

	

}
