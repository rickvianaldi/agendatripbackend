<?php namespace App\Http\Controllers\api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Agenda;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Session;

class AgendaController extends Controller {

	/**
	 * Displays all agendas belonging to the requesting user
	 *
	 * @return Response
	 */
	public function index()
	{
		$u = Session::get('user');
		$user_agendas = $u->agendas;
		return response()->json(['status' => 'ok' , 'message' => '', 'payload' => $user_agendas],200);
	}

	/**
	 * Stores a new agenda to the server, returns the newly created id.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$this->validate($request,[
			'name'=>'required',
			'startdate' => array('required','date'),
			'enddate' => array('required','date'),
			]);
		$input = $request->only('name');
		$input['start'] = $request->get('startdate');
		$input['end'] = $request->get('enddate');
		$u = Session::get('user');
		$agenda = $u->agendas()->create($input);
		return response()->json(['status' => 'ok', 'message' => '', 'payload' => ['agenda_id' => $agenda->id]],200);
	}

	/**
	 * Display the details of agenda.
	 *
	 */
	public function show(Request $request)
	{
		$input = $request->only('agenda_id');
		$u = Session::get('user');
		try
		{
			$agenda = $u->agendas()->with('agenda_details','agenda_details.transport_detail','agenda_details.vacation_site')->findOrFail($input['agenda_id']);
		}catch(ModelNotFoundException $e)
		{
			return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
		}
		return response()->json(['status' => 'ok', 'message' => '', 'payload' => $agenda],200);
	}

	/**
	 * Update the the agenda.
	 *
	 */
	public function update(Request $request)
	{
		$this->validate($request,[
			'name'=>'required'
			]);
		$input = $request->only('name','agenda_id');
		$u = Session::get('user');
		$agenda = $u->agendas()->find($input['agenda_id']);
		if($agenda->user_id == $u->id){
			$agenda->update(['name' => $input['name']]);
			return response()->json(['status' => 'ok', 'message' => '', 'payload' => ''],200);
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(Request $request)
	{
		$u = Session::get('user');
		$input = $request->only('name','agenda_id');
		$agenda = $u->agendas()->findOrFail($input['agenda_id']);
		if($agenda->user_id == $u->id){
			$agenda->delete();
			return response()->json(['status' => 'ok', 'message' => '', 'payload' => ''],200);
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}

}
