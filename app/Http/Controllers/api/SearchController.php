<?php namespace App\Http\Controllers\api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\VacationSite;

use DB;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Session;
use Searchy;

class SearchController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchByKey(Request $request){
		$input = $request['keyword'];

		if(Session::get('user')){
			$user_id = Session::get('user')->id;
		}
		else {
			$user_id = 0;
		}	

		$vacation_sites = Searchy::vacation_sites('name')->query($input)->getQuery()->join(DB::raw('(SELECT (1 / (1 + exp(-1 * SUM(IFNULL(vacation_site_tags.value,0) * IFNULL(user_tags.value,0) ))) ) as hypotheses,vacation_site_tags.vacation_site_id as vsid FROM vacation_site_tags LEFT JOIN (SELECT * FROM user_tags WHERE user_tags.user_id = '.$user_id.') as user_tags ON vacation_site_tags.tag_id = user_tags.tag_id  RIGHT JOIN vacation_sites ON vacation_sites.id = vacation_site_tags.vacation_site_id GROUP BY vacation_sites.id) as hippo'),'hippo.vsid','=','vacation_sites.id')->take(10)->get();
		return response()->json(['status' => 'ok' , 'message' => '', 'payload' => $vacation_sites],200);
	}

	public function searchByPosition(Request $request){
		$PER_PAGE = 10;

		if(Session::get('user')){
			$user_id = Session::get('user')->id;
		}
		else {
			$user_id = 0;
		}	
		
		$this->validate($request,VacationSite::$search_by_location_params);
		$lat = $request["latitude"];
		$lon = $request["longitude"];
		if(!is_null($request["treshold"]))
		{
			$treshold = $request["treshold"];	
		}
		else{
			$treshold = 10;
		}

		if(!$request->get('page'))
		{
			$page = 1;
		}
		else 
			$page = $request->get('page');
		
		$distance_query = "( sqrt(pow({$lat} - latitude,2) + pow({$lon} - longitude,2)) ) as distance";
		$vacation_sites = DB::table("vacation_sites")->select("*",DB::raw("111.1111 *
    DEGREES(ACOS(COS(RADIANS(latitude))
         * COS(RADIANS(?))
         * COS(RADIANS(longitude - ?))
         + SIN(RADIANS(latitude))
         * SIN(RADIANS(?)))) AS distance"))->join(DB::raw('(SELECT (1 / (1 + exp(-1 * SUM(IFNULL(vacation_site_tags.value,0) * IFNULL(user_tags.value,0) ))) ) as hypotheses,vacation_site_tags.vacation_site_id as vsid FROM vacation_site_tags LEFT JOIN (SELECT * FROM user_tags WHERE user_tags.user_id = '.$user_id.') as user_tags ON vacation_site_tags.tag_id = user_tags.tag_id  RIGHT JOIN vacation_sites ON vacation_sites.id = vacation_site_tags.vacation_site_id GROUP BY vacation_sites.id) as hippo'),'hippo.vsid','=','vacation_sites.id')->orderBy('distance')->havingRaw(DB::raw('distance <= ?'))->setBindings([$lat,$lon,$lat,$treshold]);

		
		$total_items = count($vacation_sites->get());
		$total_pages = floor(($total_items / $PER_PAGE)) + 1;
		if($page>$total_pages)
			$page = $total_pages;
		$vacation_sites = $vacation_sites->skip(($page-1)*$PER_PAGE)->take($PER_PAGE)->get();

		$pagination_info['currentPage']=$page;
		$pagination_info['maxPage']=$total_pages;
		$pagination_info['perPage']=$PER_PAGE;
		$pagination_info['totalItems']=$total_items;
		return response()->json(['status' => 'ok','message' => '', 'payload' => $vacation_sites,'pagination' => $pagination_info],200);

	}

	public function searchByTop(Request $request){
		if(Session::get('user')){
			$user_id = Session::get('user')->id;
		}
		else {
			$user_id = 0;
		}	

		$vacation_sites = VacationSite::orderBy('rating','DESC')->join(DB::raw('(SELECT (1 / (1 + exp(-1 * SUM(IFNULL(vacation_site_tags.value,0) * IFNULL(user_tags.value,0) ))) ) as hypotheses,vacation_site_tags.vacation_site_id as vsid FROM vacation_site_tags LEFT JOIN (SELECT * FROM user_tags WHERE user_tags.user_id = '.$user_id.') as user_tags ON vacation_site_tags.tag_id = user_tags.tag_id  RIGHT JOIN vacation_sites ON vacation_sites.id = vacation_site_tags.vacation_site_id GROUP BY vacation_sites.id) as hippo'),'hippo.vsid','=','vacation_sites.id')->take(10)->get();
		return response()->json(['status' => 'ok','message' => '', 'payload' => $vacation_sites],200);

	}

	public function searchByPreference(Request $request){
		$user_id=Session::get('user')->id;
		$vacation_sites = DB::select(DB::raw('SELECT *, IFNULL(hypotheses.hypotheses,0) as hypotheses FROM vacation_sites LEFT JOIN (SELECT (1 / (1 + exp(-1 * SUM(IFNULL(vacation_site_tags.value,0) * IFNULL(user_tags.value,0)))) ) as hypotheses,vacation_sites.id as vacation_site_id FROM vacation_site_tags LEFT JOIN (SELECT  *  FROM user_tags  WHERE user_tags.user_id = :user_id) as user_tags ON vacation_site_tags.tag_id = user_tags.tag_id RIGHT JOIN vacation_sites ON vacation_sites.id = vacation_site_tags.vacation_site_id GROUP BY vacation_site_id) as hypotheses ON hypotheses.vacation_site_id= vacation_sites.id WHERE hypotheses.hypotheses >= 0.5 ORDER BY hypotheses DESC LIMIT 10'),['user_id'=>$user_id]);
		//

		return response()->json(['status'=>'ok','message'=>'','payload'=>$vacation_sites]);
	}

}
