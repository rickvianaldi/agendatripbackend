<?php namespace App\Http\Controllers\api;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;
use App\Facades\UserAuth;
use App\Models\User;
class SessionController extends Controller {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		//validates that requests are filled
		$this->validate($request,[
			'email'=>'required|email',
			'password'=>'required'
			]);
		$input = $request->only('email','password');

		if(UserAuth::once(['password' => $input['password'],'email' => $input['email']])){
			$u = UserAuth::user();
			$userID = $u->id;
			$token = $u->create_app_key();
			return response()->json(['status' => 'ok' , 'message' => 'User Logged', 'payload' => ['key' => $token, 'userID' => $userID,'user'=>$u] ],201);
		}
			return response()->json(['status' => 'ERROR' , 'message' => 'USER_ERROR', 'payload' => [] ],500);
	}

	public function validates(Request $request){
		$input = $request->only('userID','key');
		$u = User::where(['id' => $input['userID']])->first();
		if($u && Hash::check($input['key'],$u->app_key)){
			return response()->json(['status' => 'ok', 'message' => 'Logged Out', 'payload' => []],200);
		}
		return response()->json(['status' => 'ERROR', 'message' => 'USER_ERROR', 'payload' => []],500);
	}

	public function destroy($id)
	{
		$input = $request->only('userID','key');
		$u = User::where(['id' => $input['userID']])->first();
		if($u && Hash::check($input['key'],$u->app_key)){
			$u->delete_app_key();
			return response()->json(['status' => 'ok', 'message' => 'Logged Out', 'payload' => []],200);
		}
		return response()->json(['status' => 'ERROR', 'message' => 'USER_ERROR', 'payload' => []],500);
	}

}
