<?php namespace App\Http\Controllers\api;

use App\Models\VacationSite;
use App\Models\UserWish;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;


class UserWishController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$u = Session::get('user');
		$userwish = $u->wishes()->with('vacation_site')->get();
		return response()->json(['status'=>'ok','message'=>'','payload'=>$userwish]);
	}

	public function create(Request $request)
	{
		$this->validate($request,[
			'vacation_site_id' => array('required','numeric')
			]);
		$vacation_site_id = $request->only('vacation_site_id');
		$u = Session::get('user');
		$vs = VacationSite::find($vacation_site_id)->first();
		if(is_null($vs)){
			return_401();
		}
		$userwish = UserWish::firstOrCreate(['user_id'=>$u->id,'vacation_site_id'=>$vs->id]);
		return response()->json(['status'=>'ok','message'=>'User Wish Created !','payload'=>''],200);
	}

	public function destroy(Request $request)
	{
		$this->validate($request,['user_wish_id'=>array('required','numeric')]);
		$u = Session::get('user');
		$uw = UserWish::find($request->get('user_wish_id'));
		if(!empty($uw)){
			if($uw->user_id = $u->id ){
				$uw->delete();
			}
		}
		return response()->json(['status'=>'OK','message'=>'','payload'=>''],200);
	}

	public function destroy_by_site(Request $request)
	{
		$this->validate($request,['vacation_site_id'=>array('required','numeric')]);
		$u = Session::get('user');
		$uw = UserWish::where(['vacation_site_id'=>$request->get('vacation_site_id'),'user_id' => $u->id]);
		if($uw->first()){
		   $uw->first()->delete();
		}
		return response()->json(['status'=>'OK','message'=>'','payload'=>''],200);
	}

}
