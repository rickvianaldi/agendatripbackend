<?php namespace App\Http\Controllers\api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AgendaDetail;

use Illuminate\Http\Request;
use Session;
class AgendaDetailController extends Controller {

	public function index(Request $request)
	{
		$this->validate($request,[
			'agenda_id'=>'required | numeric'
			]);
		$agenda_id = $request['agenda_id'];
		$u = Session::get('user');
		$agenda = $u->agendas()->find($agenda_id);
		if($agenda != null){
			$agenda_details = $agenda->agenda_details()->with('transport_detail','vacation_site')->get();
			if($agenda_details != null ){
				return response()->json(['status' => 'ok', 'message' => '', 'payload' => ['agenda_details' => $agenda_details]],200);
			}
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}

	public function create(Request $request)
	{
		$this->validate($request,AgendaDetail::$create_params);
		$agenda_id = $request['agenda_id'];
		$u = Session::get('user');
		$agenda = $u->agendas()->find($agenda_id);
		if($agenda != null){
			$input = $request->only('vacation_site_id','start','end');
			$input['user_id']=$u->id;
			$agenda->agenda_details()->create($input);
			return response()->json(['status' => 'ok', 'message' => '', 'payload' => [] ], 200);		
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}

	public function show(Request $request)
	{
		$agenda_detail_id = $request['agenda_detail_id'];
		$agenda_id = $request['agenda_id'];
		$u = Session::get('user');
		$agenda = $u->agendas()->find($agenda_id);
		if($agenda != null){
			$agenda_detail = $agenda->agenda_details()->with('transport_detail','vacation_site')->find($agenda_detail_id);
			if($agenda_detail != null){
				return response()->json(['status' => 'ok','message' => '', 'payload' => [$agenda_detail] ],200);
			}
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => [] ],404);
	}

	public function update(Request $request)
	{
		$this->validate($request,AgendaDetail::$update_params);
		$u = Session::get('user');
		$agenda_id = $request['agenda_id'];
		$agenda_detail_id = $request['agenda_detail_id'];
		$agenda = $u->agendas()->find($agenda_id);
		if($agenda != null) {
			$agenda_detail = $agenda->agenda_details()->find($agenda_detail_id);
			if($agenda_detail != null){
				$input = $request->only('vacation_site_id','start','end');
				$agenda_detail->update($input);
				return response()->json(['status' => 'ok','message' => '', 'payload' => [] ],200);
			}
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}

	public function destroy(Request $request)
	{
		$agenda_detail_id = $request['agenda_detail_id'];
		$agenda_id = $request['agenda_id'];
		$u = Session::get('user');
		$agenda = $u->agendas()->find($agenda_id);

		if($agenda != null){
			$agenda_detail = $agenda->agenda_details()->find($agenda_detail_id);
			if($agenda_detail != null){
				$agenda_detail->delete();
				return response()->json(['status' => 'ok', 'message' => '', 'payload' => ''],200);
			}
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}

}
