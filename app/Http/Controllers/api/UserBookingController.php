<?php namespace App\Http\Controllers\api;

use App\Models\Agenda;
use App\Models\UserBooking;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;
use Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Promise;

class UserBookingController extends Controller {

	public function showAgendaBooking(Request $request){
		$validator = Validator::make($request->all(),[
			'agenda_id' => 'required',
			]);

		if($validator->fails()){
			return response()->json($validator->errors()->all());
		}
		$u = Session::get('user');
		$agenda_id = $request->get('agenda_id');
		$agenda = Agenda::find($agenda_id);
		$url_order_check = env('TIKET_API').'/check_order';
		$count = 0;

		if($agenda && $agenda->user_id == $u->id){
			$agenda_bookings = $agenda->bookings()->get();

			$client = new \GuzzleHttp\Client();
			$promises = array();
			foreach($agenda_bookings as $ab){
				$params = array();
				$params['order_id'] = $ab->order_id;
				$params['token'] = $ab->token;
				$params['output'] = 'json';
				$params['email'] = $ab->contact_email;
				$promises[$count] = $client->requestAsync('GET',$url_order_check,['verify'=>false,'query'=>$params]);
				$count+=1;
			}
			$count = 0;
			$results = Promise\unwrap($promises);
			foreach($results as $result){
				$resobj = json_decode($result->getBody()->getContents());
				$order = $resobj->result->order__cart_detail;
				$agenda_bookings[$count]->order = $order;
				$count+=1;
			}


			return response()->json(['payload'=>$agenda_bookings]);
		}
		else{
			return return_404();
		}
	}

	public function index()
	{
		$u = Session::get('user');
		$user_bookings = $u->bookings()->get();
		$url_order_check = env('TIKET_API').'/check_order';
		$client = new \GuzzleHttp\Client();
		$promises = array();
		$count = 0;
		foreach($user_bookings as $ub){
			$params = array();
			$params['order_id'] = $ub->order_id;
			$params['token'] = $ub->token;
			$params['output'] = 'json';
			$params['email'] = $ub->contact_email;
			$promises[$count] = $client->requestAsync('GET',$url_order_check,['verify'=>false,'query'=>$params]);
			$count+=1;
		}

		$results = Promise\unwrap($promises);
		$count = 0;
		foreach($results as $result){
			$resobj = json_decode($result->getBody()->getContents());
			$order = $resobj->result->order__cart_detail;
			$user_bookings[$count]->order = $order;
			$count+=1;
		}
		//dd($user_bookings);
		return response()->json(['status'=>'ok','message'=>'','payload'=>json_decode($user_bookings)]);
		//respondwait
	}

	public function show(Request $request)
	{
		$u = Session::get('user');
		$user_booking = UserBooking::find($request->get('user_booking_id'));
		
		if(is_null($user_booking)){
			return response()->json(['status'=>'ERROR','message'=>'User Booking not found','payload'=>''],404);
		}	
			$client = new \GuzzleHttp\Client();
			$url = env('TIKET_API').'/check_order';

			$user_booking = $user_booking->first();
			$params['order_id']=$user_booking->order_id;
			$params['token'] = $user_booking->token;
			$params['email'] = $user_booking->contact_email;
			$params['output']='json';

			$res = $client->request('GET',$url,['verify'=>false,'synchronous' =>true,'query'=>$params]);
			$res = json_decode($res->getBody()->getContents());

			$user_booking->order = $res->result->order__cart_detail;

			return response()->json(['status'=>'OK','message'=>'','payload'=>$user_booking],200);
	}

}
