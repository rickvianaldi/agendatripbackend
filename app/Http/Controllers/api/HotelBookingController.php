<?php namespace App\Http\Controllers\api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\UserBooking;
use App\Models\Agenda;

use Illuminate\Http\Request;

use Session;

class HotelBookingController extends Controller {

	public function autoComplete(Request $request){
		$client = new \GuzzleHttp\Client();
		$url = env('TIKET_API').'/search/autocomplete/hotel';
		$params = $request->only('q','token');
		$params['output']='json';
		$res = $client->request('GET',$url,['verify'=>false,'synchronous'=>true,'query'=>$params]);
		return response()->json(json_decode($res->getBody()->getContents()));
	}

	public function searchHotel(Request $request){
		$client = new \GuzzleHttp\Client();
		$url = env('TIKET_API').'/search/hotel';
		$params = $request->all();
		$params['output']='json';
		$res = $client->request('GET',$url,['verify'=>false,'synchronous'=>true,'query'=>$params]);
		return response()->json(json_decode($res->getBody()->getContents()));
	}

	public function addOrder(Request $request){
		$this->validate($request,['token'=>'required',
								  'salutation' => 'required',
								  'firstName' => 'required',
								  'lastName' => 'required',
								  'phone' => 'required',
								  'conSalutation' => 'required',
								  'conFirstName' => 'required',
								  'conLastName' => 'required',
								  'conEmailAddress' => 'required',
								  'conPhone' => 'required',
								  'country' => 'required',
								  'bookUri' =>'required'
								]);

		$client = new \GuzzleHttp\Client();
		$data['token'] = $request->get('token');

		$u = Session::get('user');
		if($request->get('key') != null)
			$key = $request->get('key');
		else $key = "wrongkey";

		$res = null;
		$res_order = null;
		$result_customer_login = null;
		$result_payment_methods = null ;

		$url = urldecode($request->get('bookUri'))."&token=".$request->get('token')."&output=json";
		$res = $client->request('GET',$url,['verify' => false, 'synchronous' =>true,'timeout' => 310]);

		$result_json = json_decode($res->getBody()->getContents());
		$result_json->step='step1';
		if(property_exists($result_json->diagnostic,'confirm')){
			$url_order = env('TIKET_API').'/order';
			$param_order['token']=$data['token'];
			$param_order['output']='json';
			$res = $client->request('GET',$url_order,['verify' => false, 'synchronous' => true,'query' => $param_order]);
		//=================================================================
		// STEP 2 : RETRIEVE ORDER ID. THE ADD ORDER API DOESNT RETURN ORDER ID
		//=================================================================
			$result_json = json_decode($res->getBody()->getContents());
			$result_json->step='step2';
			if(property_exists($result_json->diagnostic,'confirm'))
			{	
		//=================================================================
		// CHECKS IF ORDER ACTUALLY EXISTS. AND GET SOME DATA		//=================================================================
				$data['order_id'] = $result_json->myorder->order_id;
				$data['order_detail_id'] = $result_json->myorder->data[0]->order_detail_id;
				$data['order_type'] = $result_json->myorder->data[0]->order_type;
				$data['order_expiration'] = $result_json->myorder->data[0]->order_expire_datetime;
				$data['order_amount'] = intval($result_json->myorder->total_without_tax);
				if(!is_null($u) && strcmp($u->app_key,$key)==0){
					$data['user_id'] = $request->get('userID');	
					if($request->get('agenda_id')!=null){
						$agenda = Agenda::find($request->get('agenda_id'));
						if(!is_null($agenda)){
							if($agenda->user_id == $u->id){
								$data['agenda_id'] = $agenda->id;
							}						
						}
					}
				}
				$data['contact_email'] = $request->get('conEmailAddress');

				$url_checkout_customer = env('TIKET_API').'/checkout/checkout_customer';


				$customer_informations['token'] = $data['token'];
				$customer_informations['salutation'] = $request->get('conSalutation');
				$customer_informations['firstName'] = $request->get('conFirstName');
				$customer_informations['lastName'] = $request->get('conLastName');
				$customer_informations['phone'] = $request->get('conPhone');
				$customer_informations['emailAddress'] = $request->get('conEmailAddress');
				$customer_informations['output']='json';
				$customer_informations['saveContinue']='2';
				$res = $client->request('GET',$url_checkout_customer,['verify' => false, 'synchronous' => true, 'query' => $customer_informations]);
				$result_json = json_decode($res->getBody()->getContents());
				$result_json->step='step3';	
				if(property_exists($result_json->diagnostic,'confirm')) {
					$url_payment_methods = env('TIKET_API').'/checkout/checkout_customer';
					$checkout_customer_info = $request->only('conSalutation','conFirstName','conLastName','conPhone','conEmailAddress','firstName','lastName','phone','salutation','country','detailId','token');
					$checkout_customer_info['output'] = 'json';
					$checkout_customer_info['detailId'] = $data['order_detail_id'];
					$res = $client->request('GET',$url_payment_methods,['verify'=>false,'synchronous' => true,'query' => $checkout_customer_info]);
					$result_json = json_decode($res->getBody()->getContents());
					$result_json->step='step4';
					if(property_exists($result_json->diagnostic,'confirm')){
						$url_payment_checkout = env('TIKET_API').'/checkout/checkout_payment/3';
						$data_payment_checkout['token'] = $data['token'];
						$data_payment_checkout['output'] ='json';
						$data_payment_checkout['btn_booking'] = '1';
						$data_payment_checkout['user_bca']='pengguna1212';
						$res = $client->request('GET',$url_payment_checkout,['verify'=>false,'synchronous' =>true,'query'=>$data_payment_checkout]);
						$result_json = json_decode($res->getBody()->getContents());
						$result_json->step='step5';
						if(property_exists($result_json->diagnostic,'confirm')){
							$data['order_amount'] = intval($result_json->grand_total);
							$user_booking = UserBooking::create($data);
							$user_booking->save();
							$result_json->booking_id=$user_booking->id;
							response()->json($result_json);
						}
					}
				}
			}
		}
		return response()->json($result_json);
	}
}
