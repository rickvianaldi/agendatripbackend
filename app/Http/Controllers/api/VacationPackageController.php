<?php namespace App\Http\Controllers\api;

use App\Models\VacationPackage;
use App\Models\VacationPackageView;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Session;
use Searchy;

use Illuminate\Http\Request;

class VacationPackageController extends Controller {

	public function fresh()
	{
		if(Session::get('user')){
			$user_id = Session::get('user')->id;
		}
		else {
			$user_id = 0;
		}	
		$vacation_package = VacationPackage::take(10)->leftJoin(DB::raw('(SELECT (1 / (1 + exp(-1 * SUM(IFNULL(vacation_package_tags.value,0) * IFNULL(user_tags.value,0) ))) ) as hypotheses,vacation_package_tags.vacation_package_id as vpid FROM vacation_package_tags LEFT JOIN (SELECT * FROM user_tags WHERE user_tags.user_id = '.$user_id.') as user_tags ON vacation_package_tags.tag_id = user_tags.tag_id  RIGHT JOIN vacation_packages  ON vacation_packages.id = vacation_package_tags.vacation_package_id GROUP BY vacation_packages.id) as hippo'),'hippo.vpid','=','vacation_packages.id')->leftJoin('vacation_package_purchases','vacation_packages.id','=','vacation_package_purchases.vacation_package_id')->select('vacation_packages.*',DB::raw('IFNULL(hippo.hypotheses,0) as hypotheses'),DB::raw('COUNT(vacation_package_purchases.id) as purchase_count'))->groupBy('vacation_packages.id')->orderBy('created_at','desc')->get();
		return response()->json(['status'=>'ok','message'=>'','payload'=>$vacation_package]);
	}

	public function hot(){
		if(Session::get('user')){
			$user_id = Session::get('user')->id;
		}
		else {
			$user_id = 0;
		}	
		$vacation_package = DB::table('vacation_packages')->leftJoin(DB::raw('(SELECT (1 / (1 + exp(-1 * SUM(IFNULL(vacation_package_tags.value,0) * IFNULL(user_tags.value,0) ))) ) as hypotheses,vacation_package_tags.vacation_package_id as vpid FROM vacation_package_tags LEFT JOIN (SELECT * FROM user_tags WHERE user_tags.user_id = '.$user_id.') as user_tags ON vacation_package_tags.tag_id = user_tags.tag_id  RIGHT JOIN vacation_packages  ON vacation_packages.id = vacation_package_tags.vacation_package_id GROUP BY vacation_packages.id) as hippo'),'hippo.vpid','=','vacation_packages.id')->leftJoin('vacation_package_purchases','vacation_packages.id','=','vacation_package_purchases.vacation_package_id')->select('vacation_packages.*',DB::raw('IFNULL(hippo.hypotheses,0) as hypotheses'),DB::raw('COUNT(vacation_package_purchases.id) as purchase_count'))->groupBy('vacation_packages.id')->take(10)->get();
		return response()->json(['status'=>'ok','message'=>'','payload'=>$vacation_package]);
	}

	public function searchByName(Request $request){
		$input = $request['keyword'];
		if(Session::get('user')){
			$user_id = Session::get('user')->id;
		}
		else {
			$user_id = 0;
		}	
		$vacation_packages = Searchy::vacation_packages('title')->query($input)->getQuery()->join(DB::raw('(SELECT (1 / (1 + exp(-1 * SUM(IFNULL(vacation_package_tags.value,0) * IFNULL(user_tags.value,0) ))) ) as hypotheses,vacation_package_tags.vacation_package_id as vpid FROM vacation_package_tags LEFT JOIN (SELECT * FROM user_tags WHERE user_tags.user_id = '.$user_id.') as user_tags ON vacation_package_tags.tag_id = user_tags.tag_id  RIGHT JOIN vacation_packages  ON vacation_packages.id = vacation_package_tags.vacation_package_id GROUP BY vacation_packages.id) as hippo'),'hippo.vpid','=','vacation_packages.id')->leftJoin(DB::raw('(SELECT (IFNULL(COUNT(vacation_package_purchases.id),0)) as purchase_count,vacation_package_purchases.id as vpid FROM vacation_package_purchases GROUP BY vacation_package_purchases.vacation_package_id ) as purch'),'purch.vpid','=','vacation_packages.id')->take(20)->addSelect(DB::raw('IFNULL(purch.purchase_count,0) as purchase_count'),DB::raw('IFNULL(hippo.hypotheses,0) as hypotheses'))->get();
		
		return response()->json(['status' => 'ok' , 'message' => '', 'payload' => $vacation_packages],200);
	}

	public function show(Request $request)
	{
		$u = Session::get('user');
		$vacation_package_id = $request->get('vacation_package_id');
		$vp = VacationPackage::find($vacation_package_id);
		if(!is_null($vp)){
			if(!is_null($u)){
			$vpv = VacationPackageView::create(['vacation_package_id'=>$vp->id,'user_id'=>$u->id]);
			}
			return response()->json(['status'=>'ok', 'message'=>'','payload'=>$vp]);
		}
		return response()->json(['status' => 'ERROR', 'message' => 'Not Found', 'payload' => ''],404);
	}

	public function searchByPreference(Request $request){
			$user_id=Session::get('user')->id;
			$vacation_packages = DB::select(DB::raw('SELECT * FROM vacation_packages JOIN (SELECT (1 / (1 + exp(-1 * SUM(IFNULL(vacation_package_tags.value,0) * IFNULL(user_tags.value,0) ))) ) as hypotheses,vacation_packages.id as vacation_package_id FROM vacation_package_tags LEFT JOIN (SELECT * FROM user_tags WHERE user_tags.user_id = :user_id) as user_tags ON vacation_package_tags.tag_id = user_tags.tag_id  RIGHT JOIN vacation_packages  ON vacation_packages.id = vacation_package_tags.vacation_package_id GROUP BY vacation_packages.id) as hypotheses ON hypotheses.vacation_package_id= vacation_packages.id WHERE hypotheses.hypotheses >= 0.5 ORDER BY hypotheses DESC LIMIT 10'),['user_id'=>$user_id]);

			return response()->json(['status'=>'ok','message'=>'','payload'=>$vacation_packages]);
	}
}
