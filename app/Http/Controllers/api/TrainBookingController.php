<?php namespace App\Http\Controllers\api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\UserBooking;
use App\Models\Agenda;

use Illuminate\Http\Request;

use Session;

class TrainBookingController extends Controller {
	
	public function getStations(Request $request){
		$this->validate($request,['token'=>'required']);
		$token = $request->get('token');
		$client = new \GuzzleHttp\Client();
		$url = 'https://api-sandbox.tiket.com/train_api/train_station?token='.$token.'&output=json';
		$res = $client->request('GET',$url,['verify' => false,'synchronous' => true]);
		return response()->json(json_decode($res->getBody()->getContents()),$res->getStatusCode());
	}

	public function getTrains(Request $request){
		$this->validate($request,[
			'a'=>'required',
			'd'=>'required',
			'date'=>array('required','regex:/\d{4}-\d{2}-\d{2}/'),
			'ret_date'=>'regex:/\d{4}-\d{2}-\d{2}/',
			'adult'=>array('required','numeric','min:1'),
			'child'=>array('required','numeric','min:0'),
			'infant'=>array('required','numeric','min:0'),
			'class'=>'regex:/all|bis|eks|eco/',
			'token'=>'required',
		]);
		$params = $request->only('a','d','date','adult','child','infant','token','class');
		if($request->has('ret_date')){
			$params['ret_date']=$request->get('ret_date');
		}
		$params['output']='json';
		$client = new \GuzzleHttp\Client();
		$url = 'https://api-sandbox.tiket.com/search/train';
		$res = $client->request('GET',$url,['verify'=>false,'synchronous' => true,'query'=>$params]);
		return response()->json(json_decode($res->getBody()->getContents()),$res->getStatusCode());
	}

	public function addOrder(Request $request){
		$client = new \GuzzleHttp\Client();
		$data['token'] = $request->get('token');
		$params = $request->all();
		$params['output']='json';

		$u = Session::get('user');
		if($request->get('key') != null)
			$key = $request->get('key');
		else $key = "wrongkey";

		$url = env('TIKET_API').'/order/add/train';
		$res = null;
		$res_order = null;
		$result_customer_login = null;
		$result_payment_methods = null;
		$res = $client->request('GET',$url,['verify'=>false,'synchronous'=>true,'query' => $params,'timeout' =>310]);
		$result_json = json_decode($res->getBody()->getContents());
		$result_json->step='step1';
		if(property_exists($result_json->diagnostic,'confirm')){
			$url_order = env('TIKET_API').'/order';
			$param_order['token']=$data['token'];
			$param_order['output']='json';
			$res = $client->request('GET',$url_order,['verify'=>false,'synchronous'=>true,'query'=>$param_order]);

			$result_json = json_decode($res->getBody()->getContents());
			$result_json->step='step2';
			if(property_exists($result_json->diagnostic,'confirm')){
				$data['order_id'] = $result_json->myorder->order_id;
				$data['order_detail_id'] = $result_json->myorder->data[0]->order_detail_id;
				$data['order_type'] = $result_json->myorder->data[0]->order_type;
				$data['order_amount'] = intval($result_json->myorder->total_without_tax);
				if(!is_null($u)&&strcmp($u->app_key,$key)==0){
					$data['user_id'] = $request->get('userID');	
					if($request->get('agenda_id')!=null){
						$agenda = Agenda::find($request->get('agenda_id'));
						if(!is_null($agenda)){
							if($agenda->user_id == $u->id){
								$data['agenda_id'] = $agenda->id;
							}						
						}
					}
				}
				$data['contact_email'] = $request->get('conEmailAddress');

				$url_checkout_customer = env('TIKET_API').'/checkout/checkout_customer';

				$customer_informations['token'] = $data['token'];
				$customer_informations['salutation'] = $params['conSalutation'];
				$customer_informations['firstName'] = $params['conFirstName'];
				$customer_informations['lastName'] = $params['conLastName'];
				$customer_informations['phone'] = $params['conPhone'];
				$customer_informations['emailAddress'] = $params['conEmailAddress'];
				$customer_informations['output']='json';
				$customer_informations['saveContinue']='2';
				$res = $client->request('GET',$url_checkout_customer,['verify' => false, 'synchronous' => true, 'query' => $customer_informations]);
				$result_json = json_decode($res->getBody()->getContents());
				$result_json->step='step3';	
				if(property_exists($result_json->diagnostic,'confirm')) {
					$url_payment_methods = env('TIKET_API').'/checkout/checkout_payment';
					$data_payment_method['token'] = $data['token'];
					$data_payment_method['output'] = 'json';
					$res = $client->request('GET',$url_payment_methods,['verify'=>false,'synchronous' => true,'query' => $data_payment_method]);
					$result_json = json_decode($res->getBody()->getContents());
					$result_json->step='step4';
					if(property_exists($result_json->diagnostic,'confirm')){
						$url_payment_checkout = env('TIKET_API').'/checkout/checkout_payment/3';
						$data_payment_checkout['token'] = $data['token'];
						$data_payment_checkout['output'] ='json';
						$data_payment_checkout['btn_booking'] = '1';
						$data_payment_checkout['user_bca']='pengguna1212';
						
						$res = $client->request('GET',$url_payment_checkout,['verify'=>false,'synchronous' =>true,'query'=>$data_payment_checkout]);
						$result_json = json_decode($res->getBody()->getContents());
						$result_json->step='step5';
						if(property_exists($result_json->diagnostic,'confirm')){
							$data['order_amount'] = intval($result_json->grand_total);
							$user_booking = UserBooking::create($data);
							$user_booking->save();
							$result_json->booking_id=$user_booking->id;
							response()->json($result_json);
						}
					}
				}
			}
		}
		return response()->json($result_json);
	}
}
