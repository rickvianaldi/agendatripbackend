<?php 
namespace App\Http\Controllers\api;

use App\Models\UserBooking;
use App\Models\VacationPackagePurchase;
use App\Models\UserPayment;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use Veritrans_Config;
use Veritrans_VtWeb;
use Veritrans_Notification;

class PaymentController extends Controller {

    public function __construct(){   
        Veritrans_Config::$serverKey = env('VERITRANS_SERVER');
        Veritrans_Config::$isProduction = false;
    }

	public function pay(Request $request){
		$validator = Validator::make($request->all(),[
			'order_id' => 'required',
			'order_type' => 'required'
			]);

		if($validator->fails()){
			return response()->json($validator->errors()->all());
		}
		$params = $request->only('order_id','order_type');
		$order = null;
		switch($params['order_type']){
			case 'package' : 
				$order = VacationPackagePurchase::find($params['order_id']);
			break;
			case 'booking' : 
				$order = UserBooking::find($params['order_id']);	
			break;
		}
		
		if(is_null($order)){
			return response()->json(['status'=>'ERROR','message'=>'No such order exists'],404);
		}

		switch($params['order_type']){
			case 'package' : 
				$params['order_id']='VP'.$params['order_id'];
			break;
			case 'booking' : 
				$params['order_id']='BO'.$params['order_id'];	
			break;
		}

		$params['gross_amount'] = $order->order_amount;
		$transaction['transaction_details']=$params;
		$transaction['order_type'] = $params['order_type'];
		try{
			$vtweb_url = Veritrans_VtWeb::getRedirectionUrl($transaction);
			return redirect($vtweb_url);
		}
		catch(\Exception $e){
			return view('payment/error');
		}
	}

	public function callback(Request $request){

		
		$upParams = array();
		$notif = json_decode(json_encode($request->all()));
		$statusCode = $notif->status_code;
		$transaction_id = $notif->transaction_id;
		$transaction_status = $notif->transaction_status;
		$transaction_time = $notif->transaction_time;
		$type = $notif->payment_type;
		$order_id = $notif->order_id;
		if(substr($order_id,0,2)=='BO'){
			$order_type='user_booking';
		}
		else if(substr($order_id,0,2)=='VP'){
			$order_type="vacation_package_purchase";
		}
		$signature_key = $notif->signature_key;
		$amount = $notif->gross_amount;

		$signature = openssl_digest($order_id.$statusCode.$amount.env('VERITRANS_SERVER'),'sha512');

		if(strcmp($signature_key,$signature)!=0){
			return return_401();
		}

		if($transaction_status == 'capture'){
			if($type == 'credit_card'){
				$fraud = $notif->fraud_status;
				if(strcasecmp($fraud,'challenge')==0){
					$upParams['transaction_status']='pending';
				}
				else if(strcasecmp($fraud,'accept')==0){
					$upParams['transaction_status']='success';
				}
				else{
					$upParams['transaction_status']='denied';
				}
			}
		}
		else if(strcasecmp($transaction_status,'settlement')==0){
			$upParams['transaction_status']='success';
		}
		else{
			$upParams['transaction_status']=$transaction_status;
		}
		if($upParams['transaction_status'] == 'success'){
			$id=substr($order_id,2,strlen($order_id));
			if($order_type == 'user_booking'){
				$order = UserBooking::find($id);
			}
			else if($order_type == 'vacation_package_purchase'){
				$order = VacationPackagePurchase::find($id);
			}
			$order->update(['status'=> 'paid']);
			$status = $order->save();
		}
		$upParams['order_id'] = substr($order_id,2,strlen($order_id));
		$upParams['order_type'] = $order_type;
		$upParams['order_amount'] = $amount;
		$upParams['transaction_id'] = $transaction_id;
		$upParams['transaction_time'] = $transaction_time;
		$upParams['payment_type'] = $type;
		$upParams['payment_currency'] = 'IDR';
		$upParams['signature_key'] = $signature_key;
		$user_payment = UserPayment::where(['transaction_id'=>$upParams['transaction_id']])->first();
		if($user_payment){
			$user_payment->update(['transaction_status'=>$upParams['transaction_status']]);
			$user_payment->save();
		}
		else{
			$user_payment = UserPayment::create($upParams);
		}
	}

	public function success(){
		return view('payment/success');
	}

	public function error(){
		return view('payment/error');
	}
}
