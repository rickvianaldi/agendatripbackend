<?php namespace App\Http\Controllers\api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\VacationPackagePurchase;
use App\Models\VacationPackage;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Carbon\Carbon;
class VacationPackagePurchaseController extends Controller {

	public function index()
	{
		$user = Session::get('user');
		$vacation_package_purchases = $user->vacation_package_purchases()->
		with(array('vacation_package'=>function($query){
			$query->select('id','title','image','date_start');
		}
		))->get();
		return response()->json(['status' => 'ok', 'message' => '', 'payload' => $vacation_package_purchases],200);
	}

	public function create(Request $request)
	{
		$user = Session::get('user');
		$vacation_package_id = $request->only('vacation_package_id')['vacation_package_id'];
		if($vacation_package_id == null){
			 return response()->json(['status' => 'ERROR','message' =>'not found', 'payload' =>''],404);
		}
		$vacation_package = VacationPackage::find($vacation_package_id)->first();
		if($vacation_package == null){
			 return response()->json(['status' => 'ERROR','message' =>'not found', 'payload' =>''],404);
		}
    $params = array();
    $params['user_id'] = $user->id;
    $params['vacation_package_id']= $vacation_package_id;
    $params['order_amount']=$vacation_package->price;
    $params['reference_code'] = md5("1" . $vacation_package_id . Carbon::now()->toDateTimeString() . str_random(8));
    $vacation_package_purchase = VacationPackagePurchase::create($params);
    $vacation_package_purchase = $vacation_package_purchase::with('vacation_package')->find($vacation_package_purchase->id);
		return response()->json(['status' => 'ok','message' => '','payload' => $vacation_package_purchase],200);
	}

	public function show(Request $request)
	{
		$vacation_package_purchase_id = $request->only('vacation_package_purchase_id')['vacation_package_purchase_id'];
		if($vacation_package_purchase_id == null){
			 return response()->json(['status' => 'ERROR','message' =>'not found', 'payload' =>''],404);
		}
		$vacation_package_purchase = VacationPackagePurchase::with('vacation_package')->find($vacation_package_purchase_id);
		if($vacation_package_purchase == null || $vacation_package_purchase->user_id != Session::get('user')->id){
			 return response()->json(['status' => 'ERROR','message' =>'not found', 'payload' =>''],404);
		}
		return response()->json(['status' => 'ok', 'message' => '', 'payload' => $vacation_package_purchase]);
	}


}
