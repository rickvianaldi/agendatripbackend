<?php  
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Input;
use Validator;
use Request;
use Response;
use Image;
use App\Models\VacationSite;
use App\Models\VacationSitePhoto;





class DropzoneController extends Controller {

    public function uploadFiles($id) {
        //shorthands & initialization  
                $input = Input::all();
                $img = Image::Make(Input::file('file')->getRealPath());
                $upload_success = false;

        //validation
                $rules = array(
                    'file' => 'image|max:2000', //BUG - this is launched after upload, silly .... files rejected after upload
                );
         
                $validation = Validator::make($input, $rules);

                if ($validation->fails()) {
                    return Response::make($validation->errors()->first(), 400);
                }

        //initialize file identity
                $destinationPath = 'vacationimages'; // upload path
                //$extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
                $extension = 'jpg'; // setting desired file extension
                $fileName = 'vs_'.$id."_".date('Ymd_h'); // variable containing image postfix : [id]_[tanggal & jam] 
                
                $vacationsite = VacationSite::findOrFail($id);
                $photo = new VacationSitePhoto;
            

        
        if ($img) { //img valid?

            //saving uploaded image to database
                $vacationsite->photos()->save($photo); // save using relation, to get the ID first

                $fileName = $fileName.'_'.$photo->id; //adding ID to the filename for uniqueness
                $photo->imageurl = $fileName.'.'.$extension; //add filename to database
                $vacationsite->photos()->save($photo); //resave for filename


            //begin selecting oversized     
                     $resized = false;

                    if($img->height() > 600){ //oversized
                       $img->resize(null, 600, function($constraint){
                            $constraint->aspectRatio();
                        })->save('vacationimages/'.$fileName.'.' . $extension,100);
                       $resized = true;
                    }

                    if($img->width() > 800){ // oversized
                       $img->resize(800, null, function($constraint){
                            $constraint->aspectRatio();
                        })->save('vacationimages/'.$fileName.'.' . $extension,100);
                       $resized = true;
                }

             //if not resized    
                    if($resized == false)
                        {$upload_success = Input::file('file')->move($destinationPath, 'vacationimages/'.$fileName.'.' . $extension);
                    }

                    
            //creating smallthumb and cover luar
                $smallthumb = $img->fit(100, 100)->save('small_vs_thumbs/ts_'.$fileName.'.'.$extension); //creating small thumbnail
                //creating moderate thumb
                $moderatethumb = $img->fit(200, 200)->save('moderate_vs_thumbs/tm_'.$fileName.'.' . $extension); //generate moderate thumb
                //creating cover image
                if($vacationsite->image == "" or is_null($vacationsite->image)){ //auto cover setting naming
                    $vacationsite->image = $fileName.'.'.$extension ; $vacationsite->save();  //naming cover menggunakan thumbnail kecil
                }

            return Response::json("success", 200); 
        } else {
            return Response::json("upload not successful", 400);
        }
    }
 
}