<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;

//models
use App\Models\VacationSite;
use App\Models\VacationSitePhoto;
use App\Models\Tag;
use App\Models\VacationSiteTag;
use Carbon;

use Input;
use Validator;
use Redirect;
use Session;
use File;
use Response;

class AdminVacationSiteController extends Controller {

	public function getVacationSiteList(){// pagination variable
		$css  = ['vendor/lightbox/lightbox'];
	
		$js_bottom = ['vendor/lightbox'];

	// pagination
		$perpage = Request::get('perpage');
		if($perpage < 5 || $perpage > 100){
			$perpage = 10;
		}

	//if there is search, then do search
		$query = Request::get('search');
		if(!empty($query)){
			$vacationlist = VacationSite::where('name','like', '%'.$query.'%')->paginate($perpage);
		}else{
			$vacationlist = VacationSite::paginate($perpage);
		}
		$search['string'] = $query;

	 //get
		foreach($vacationlist as $row){
			$tags[$row->id] = VacationSiteTag::where('vacation_site_id','like',$row->id)->get();
	}

		return view('admin.AdminVacationSiteListShow',compact('vacationlist','search','css','js','js_bottom','tags'));
	}

	public function getVacationSite($id){ //GET SINGLE VACATION SITE DATA
		$css  = ['vendor/lightbox/lightbox'];
		$js_bottom = ['vendor/lightbox'];
		$mapscript = true;

		$vacationsite = VacationSite::findOrFail($id);
		$vacationsitephotos = VacationSitePhoto::select('imageurl')->where('vacation_site_id','=',$id)->get();
		return view('admin.AdminVacationSiteShow',compact('vacationsite','vacationsitephotos','css','js_bottom','mapscript'));
	}


	public function editVacationSite($id)
	{	
		$js = [
				'vendor/dropzone',
				];

		$js_bottom = ['vendor/lightbox'];

		$css = ['vendor/basic',
				'vendor/dropzone',
				'vendor/lightbox/lightbox',
				'vacationsiteedit',
				'vendor/jquery.tagit',
				'vendor/tagit.ui-zendesk',
				'mappicker'];
		
		$vacationsite = VacationSite::findOrFail($id);
		$vacationsitephotos = VacationSitePhoto::select('imageurl')->where('vacation_site_id','=',$id)->get();

		$tags = VacationSiteTag::where('vacation_site_id','=',$id)->get();

		return view('admin.AdminVacationSiteEdit',compact('vacationsite','vacationsitephotos','tags','js','js_bottom','css','id'));
	}



	public function createVacationSite() // get method
	{	

		$css = ['vendor/basic',
				'vendor/dropzone',
				'vendor/lightbox/lightbox',
				'vacationsiteedit',
				'vendor/jquery.tagit',
				'vendor/tagit.ui-zendesk',
				'mappicker'];
		$mapscript = true;

		return view('admin.AdminVacationSiteCreate',compact('js','css','mapscript'));
	}


	public function storeVacationSite(Requests\AdminVacationSiteRequest $request)
	{

		$input = $request->only(['name','address','contactnumber','description','longitude','latitude']);
	
		//image upload
		$file = ['image'=> Input::file('image')];
		$rules = [];
		$imagevalidator = Validator::make($file, $rules);

		//BEGIN tag saving
			$taginput = $request->only('tag');
			$tags = explode(",",$taginput['tag']); //separate all from comma

			$data = new Vacationsite;
			$data->name = $input['name'];
			$data->address = $input['address'];
			$data->contactnumber = $input['contactnumber'];
			$data->description = $input['description'];
			$data->longitude = $input['longitude'];
			$data->latitude = $input['latitude'];

			if($data->save()){
				$newid =  $data->id; //ambil id
			}
			foreach ($tags as $tag){
				//check if tag exist
					if ($tag!=''){ //if it is not empty
						if(Tag::where('name','like',$tag)->exists()){		
							//exist
							$existingtagid = Tag::where('name','like',$tag)->first()->id;
						}else{
							//not exist
							$existingtagid = Tag::create(['name'=>$tag])->id;
						}
			
					VacationSiteTag::create(['tag_id' => $existingtagid,'vacation_site_id' => $newid,'value' => 1 ]); //associate new tag with the vacationsite
				
					}
					

			}
		//END of tag saving


		
		// if ($imagevalidator->fails()) {
		//     // send back to the page with the input data and errors
		//     return redirect('admin/vacationsite/create')->withInput()->withErrors($validator);
		// }
		// else 
		// {
		// 	$data = new Vacationsite;
		// 	$data->name = $input['name'];
		// 	$data->address = $input['address'];
		// 	$data->contactnumber = $input['contactnumber'];
		// 	$data->description = $input['description'];
		// 	if($data->save()){
		// 		$insertedid =  $data->id; //ambil id untuk naming gambar
		// 	}

		//     // checking file is valid.
		//     if(Input::file('image'))
		//     {	
		// 	    if (Input::file('image')->isValid()) {
		// 		    $destinationPath = 'uploads'; // upload path in public
		// 		    $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
		// 		    $fileName = 'st_'.$insertedid.'_'.$data->name.'.'.$extension; // renameing image
		// 		    Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
		// 		    // sending back with message
		// 		    Session::flash('success', 'Upload successfully'); 

		// 		    $data->image = $fileName;
		// 		    $data->save();
		// 		    return redirect('admin/vacationsite/create');
		// 	    }
		// 	    else 
		// 	    {
		// 	      // sending back with error message.
		// 	      Session::flash('error', 'uploaded file is not valid');
		// 	      return redirect('admin/vacationsite/create');
		// 	    }

		// 	}
		// }
		return redirect('admin/vacationsite');
	}



	public function updateVacationSite($id,Requests\AdminVacationSiteRequest $request)
	{	
		$input = VacationSite::findOrFail($id);
		$input->update($request->only(['name','address','contactnumber','description','longitude','latitude']));
	
		//BEGIN tag saving module
			$taginput = $request->only('tag');
			$tags = explode(",",$taginput['tag']); //separate all from comma

			foreach ($tags as $tag){
				//deleting tags that are removed
					$deltags = VacationSiteTag::where('vacation_site_id','=',$id)->get(); // ambil tag yang ada di database
					foreach($deltags as $deltag){						
						array_walk($tags, function(&$value){$value = strtolower($value);});// strtolower so checking database will be case sensitive
						
						if(!in_array(strtolower($deltag->tag->name),$tags)){ //if database tag has gone from edit form, then...
							VacationSiteTag::where('vacation_site_id','=',$id)->where('tag_id','=',$deltag->tag_id)->delete();
						}
					}

				//check if tag exist
					if ($tag!=''){ //if it is not empty
						if(Tag::where('name','like',$tag)->exists()){		
							//exist
							$existingtagid = Tag::where('name','like',$tag)->first()->id;
						}else{
							//not exist
							$existingtagid = Tag::create(['name'=>$tag])->id;
						}
					
				//check if vacationsite associated has tag or not
						if(!VacationSiteTag::where('vacation_site_id','=',$id)->where('tag_id','=',$existingtagid)->exists()){
							VacationSiteTag::create(['tag_id' => $existingtagid,'vacation_site_id' => $id,'value' => 1 ]); //associate new tag with the vacationsite
						}
					}
					

			}
		//END of tag saving module

		$input['updated_at'] = Carbon::now(); 
		return redirect('admin/vacationsite');
	}

	public function deleteVacationsite()
	{	
		
		$id = Request::only('id'); // mencegah request bentuk lain untuk lewat
		$user = Vacationsite::where('id',$id)->delete();
		// $user = User::destroy($id);
		return redirect('admin/vacationsite');
	}

	public function deleteSingleVacationSiteImage(){

		$input = Request::only('filename','vacationsiteid');
		
		$filename = $input['filename'];
		$vacationsiteid = $input['vacationsiteid'];



		if (file_exists("vacationimages/".$filename)) {
		
			$exploded = explode(".", substr(strrchr($filename, "_"), 1), 2);
			$id = $exploded[0];

			//auto switch the cover
			VacationSitePhoto::findOrFail($id)->delete();
			$coverimage = VacationSite::find($vacationsiteid);
			$coverid = explode(".", substr(strrchr($coverimage->image, "_"), 1), 2);

			if ($coverid[0] == $id){ //if the deleted image is deleted image, then switch another image
				 $anotherphoto = VacationSitePhoto::where('vacation_site_id','=',$vacationsiteid)->first();
				if(!is_null($anotherphoto)){
					$coverimage->image = $anotherphoto->imageurl;//switch
				}else{
					$coverimage->image = "";//empty the cover
				}
				$coverimage->save();
			}

			  unlink("vacationimages/".$filename);
			  unlink("small_vs_thumbs/ts_".$filename);
			  unlink("moderate_vs_thumbs/tm_".$filename);
			 return Redirect::back();

		  } else {
		    echo 'Could not delete '.$filename.', file does not exist';
		  }
	}

// //searches controllers
// 	public function searchVacationSiteByName(Request $request){
// 		$perpage = Request::get('perpage');
// 		if($perpage < 5 || $perpage > 100){
// 			$perpage = 10;
// 		}

// 		$query = Request::get('search');

	
// 		return view('admin.AdminVacationSiteSearchResult');
// 	}
}
