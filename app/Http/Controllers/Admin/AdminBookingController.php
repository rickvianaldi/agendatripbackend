<?php namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Request;

use App\Models\UserBooking;

class AdminBookingController extends Controller {	

	public function getBookingList()
	{
		$perpage = Request::get('perpage');
		if($perpage < 5 || $perpage > 100){$perpage = 10;} //default pagination
		
		$showoptions['status'] = Request::get('status');
		if(is_null($showoptions['status'])){$showoptions['status']='paid';}
	

		if($showoptions['status']!='all')
			{
				$bookinglist = UserBooking::where('status',$showoptions['status'])->orderBy('created_at','desc')->paginate($perpage);}
			else{
				$bookinglist = UserBooking::orderBy('created_at','desc')->paginate($perpage);
			}

		return view('admin.AdminBookingListShow',compact('bookinglist','showoptions'));
	}

}
