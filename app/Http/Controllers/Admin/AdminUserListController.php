<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;// i have to change this || Because of this, to use the Request facade in a namespaced file, you need to specify to use the base class: use Request;.
use App\Models\User;
use App\Models\Agenda;
use Carbon;

//models
use App\Models\UserBooking;

class AdminUserListController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	//all getters goes here
	public function getUserList(){

		$perpage = Request::get('perpage');
			if($perpage < 5 || $perpage > 100){$perpage = 10;} //default pagination
		$userlist = User::latest('id')->paginate($perpage);

	//if there is search, then do search
		$query = Request::get('search');
		if(!empty($query)){
			$userlist = User::where('email','like', '%'.$query.'%')->paginate($perpage);
		}else{
			$userlist = User::paginate($perpage);
		}
		$userlist->searchstring = $query;
		
		return view('admin.AdminUserListShow',compact('userlist'));
	}

	public function getUserData($id){ //displaying one user data
		$userdata = User::findOrFail($id);
		$useragenda = Agenda::where('user_id','=',$id);
		return view('admin.AdminUserDataShow',compact('userdata','useragenda'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function createUser()
	{
		return view('admin.AdminUserListCreate');
	}



	public function updateUser($id,Requests\AdminUserUpdate $request)
	{
		$input = User::findOrFail($id);
		$input->update($request->only(['password','firstname','lastname','email']));
		$input['updated_at'] = Carbon::now();

		return redirect('admin/user');
	}



	public function deleteUser()
	{	
		
		$id = Request::only('id'); // mencegah request bentuk lain untuk lewat
		$user = User::where('id',$id)->delete();
		// $user = User::destroy($id);
		return redirect('admin/user');
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeUser(Requests\AdminUserCRUD $request)
	{

		$input = $request->only(['password','firstname','lastname','email']);
		$input['created_at'] = Carbon::now();
		$input['updated_at'] = Carbon::now();

		User::create($input);

		// dd($input);
	 	// User::create([
			// 'username'=>$input['username'],
			// 'password'=>$input['password'],
			// 'firstname'=>$input['firstname'],
			// 'lastname'=>$input['lastname'],
			// 'email'=>$input['email']
			// ]);

		return redirect('admin/user');
		
	}

	public function getUserBookingList($id){
		$username =  User::find($id)->email;
		$perpage = Request::get('perpage');
			if($perpage < 5 || $perpage > 100){$perpage = 10;} //default pagination
		$showoptions['status'] = Request::get('status');
		if(is_null($showoptions['status'])){$showoptions['status']='paid';}

		$bookinglist = UserBooking::where('user_id',$id);
		if($showoptions['status']!='all'){$bookinglist = $bookinglist->where('status',$showoptions['status']);}
		$bookinglist = $bookinglist->orderBy('created_at','desc')->paginate($perpage);

		return view('admin.AdminUserBookingListShow',compact('username','bookinglist','showoptions'));
	}

}
