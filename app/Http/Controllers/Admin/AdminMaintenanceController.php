<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Request;
use App\Models\VacationSite;
use App\Models\Tag;
use App\Models\VacationSiteTag;
use App\Models\VacationSitePhoto;

use App\Models\VacationPackageTag;
use App\Models\VacationPackage;

use DB;
use App\Models\UserCollectData;
use App\Models\UserCollectDataResponse;

class AdminMaintenanceController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function generateVacationSiteSeed()
	{	
		$data = VacationSite::all();
		return view('maintenance.GenerateVacationSiteSeed',compact('data'));
	}

	public function generateTagSeed()
	{	
		$data = Tag::all();
		return view('maintenance.GenerateTagSeed',compact('data'));
	}
 
	public function generateVacationSiteTagSeed()
	{	
		$data = VacationSiteTag::all();
		return view('maintenance.GenerateVacationSiteTagSeed',compact('data'));
	}

	public function generateVacationSitePhotoSeed()
	{	
		$data = VacationSitePhoto::all();
		return view('maintenance.GenerateVacationSitePhotoSeed',compact('data'));
	}

//vacation packages
	public function generateVacationPackage()
	{	
		$data = VacationPackage::all();
		return view('maintenance.generatevacationpackage',compact('data'));
	}
	public function generateVacationPackageTag()
	{	
		$data = VacationPackageTag::all();
		return view('maintenance.generatevacationpackagetag',compact('data'));
	}

// usercollectdata controllers
	public function usercollectdata(){
		$userlist = UserCollectData::all();
		return view('maintenance.UserCollectDataListShow',compact('userlist'));
	}

	public function usercollectdatastore(Request $request){

		$usercreate = Request::only('username');
		UserCollectData::create($usercreate);
		return redirect('collectdata');
	}


	public function usercollectdatadelete(){
		$id = Request::only('id');
		UserCollectData::where('id',$id)->delete();
		return redirect('collectdata');
	}


	public function usercollectdataresponseform($id){
		$css  = ['vendor/lightbox/lightbox'];
		$js = ['vendor/lightbox'];

		$vacationlist = VacationSite::get();
		$user = UserCollectData::findOrFail($id);
		
		foreach($vacationlist as $row){
			$tags[$row->id] = VacationSiteTag::where('vacation_site_id','like',$row->id)->get();
		}
	
		return view('maintenance.UserCollectDataResponseForm',compact('vacationlist','user','tags','js','css'));
	}

	public function usercollectdatastoreresponse(){
		$response = Request::all();
		$userid = $response['respondenid'];

			if(UserCollectDataResponse::where('id','=',$userid)->exists()){
				
				 UserCollectDataResponse::where('id','=',$userid)->delete();
			
			}

			foreach($response as $res=>$item){
			 if(is_numeric($res)){
				 UserCollectDataResponse::create(['id'=>$userid ,'vacation_site_id'=>$res,'like'=>$item]);
				}
			}	
		return view('maintenance.UserCollectDataResponseFormSuccess');

	}

	public function tagvalues(){
		$query = DB::select(DB::raw('SELECT v.id as vacation_site_id , ti.id as tag_id , IFNULL(tage.value,0) as value FROM vacation_sites as v CROSS JOIN ( SELECT DISTINCT id from tags t) ti LEFT JOIN ( select distinct t.id as tag_id, vacation_site_id, value FROM tags t JOIN vacation_site_tags vst ON t.id = vst.tag_id) tage on v.id = tage.vacation_site_id AND ti.id = tage.tag_id'));

		$vs_ps_pr = -1;
		
			foreach($query as $row){
				if($vs_ps_pr != $row->vacation_site_id){			
					print('<br>');
				}else{
					print(' ');
				}
				print($row->value);
				$vs_ps_pr = $row->vacation_site_id;
			}

	}

	public function usercollectdataresult($id){
		//$site = Vacationsite::get();
		$responses = UserCollectDataResponse::where('id','=',$id)->get();
		foreach($responses as $row){
			print($row->like);
			print('<br>');
		}
	}

	//randomize tag value
	public function randomvalue(){
		$vst = VacationSiteTag::all();
		foreach($vst as $v){
			$v->update(['value' => 0.5 + rand(1,25) / 100 ]);
		}
		return ('success');
	}

}
