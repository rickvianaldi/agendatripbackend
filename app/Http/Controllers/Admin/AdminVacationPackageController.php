<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;

//Models
use App\Models\VacationPackage;
use App\Models\Tag;
use App\Models\VacationPackageTag;

use Carbon;
class AdminVacationPackageController extends Controller {

	public function getVacationPackageList()
	{	

	// pagination
		$perpage = Request::get('perpage');
		if($perpage < 5 || $perpage > 100){
			$perpage = 10;
		}
		
		//if there is search, then do search
		$query = Request::get('search');
		if(!empty($query)){
			$vacationpackages = VacationPackage::where('title','like', '%'.$query.'%')->paginate($perpage);
		}else{
			$vacationpackages = VacationPackage::latest('id')->paginate($perpage);
		}
		$search['string'] = $query;
		$vacationpackages->perpage($perpage);

		foreach($vacationpackages as $row){
			$tags[$row->id] = VacationPackageTag::where('vacation_package_id','like',$row->id)->get();
		}

		return view('admin.AdminVacationPackageListShow',compact('vacationpackages','tags','search'));
	}

	public function getVacationPackage($id){ //get single vacation package information
		$vacationpackage = VacationPackage::findOrFail($id);
		$tags = VacationPackageTag::where('vacation_package_id','=',$id)->get();
		return view('admin.AdminVacationPackageShow',compact('vacationpackage','tags'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function createVacationPackage()
	{   $css = ['vendor/jquery.bootstrap-touchspin',
				'vendor/jquery.tagit',
				'vendor/tagit.ui-zendesk',];

		$js = ['vendor/jquery.bootstrap-touchspin',
				'vendor/tag-it.min'];
		return view('admin.AdminVacationPackageCreate',compact('css','js'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeVacationPackage(Requests\AdminVacationPackageRequest $request)
	{
		$input = $request->all();
		//$request->only(['title','quota','header','price','sale_start','sale_end','date_start','date_end','facility_exclusion','facility_inclusion','itinenary','notes','terms']);
			$data = new VacationPackage;

			// $data->name = $input['name'];
			// $data->address = $input['address'];
			// $data->contactnumber = $input['contactnumber'];
			// $data->description = $input['description'];

			$newid = $data->create($request->except('tag'));
			$id = $newid->id;
			//BEGIN tag saving module
			$taginput = $request->only('tag');
			$tags = explode(",",$taginput['tag']); //separate all from comma
			
			
			foreach ($tags as $tag){
				//deleting tags that are removed
					$deltags = VacationPackageTag::where('vacation_package_id','=',$id)->get(); // ambil tag yang ada di database
					foreach($deltags as $deltag){						
						array_walk($tags, function(&$value){$value = strtolower($value);});// strtolower so checking database will be case sensitive
						
						if(!in_array(strtolower($deltag->tag->name),$tags)){ //if database tag has gone from edit form, then...
							VacationPackageTag::where('vacation_package_id','=',$id)->where('tag_id','=',$deltag->tag_id)->delete();
						}
					}

				//check if tag exist
					if ($tag!=''){ //if it is not empty
						if(Tag::where('name','like',$tag)->exists()){		
							//exist
							$existingtagid = Tag::where('name','like',$tag)->first()->id;
						}else{
							//not exist
							$existingtagid = Tag::create(['name'=>$tag])->id;
						}
					
				//check if vacationsite associated has tag or not
						if(!VacationPackageTag::where('vacation_package_id','=',$id)->where('tag_id','=',$existingtagid)->exists()){
							VacationPackageTag::create(['tag_id' => $existingtagid,'vacation_package_id' => $id,'value' => 1]); //associate new tag with the vacationsite
						}
					}
			}
		//END of tag saving module
	
		return redirect('admin/vacationpackage');
	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editVacationPackage($id)
	{
		$js = ['vendor/dropzone',
				'vendor/lightbox',
				'vendor/tag-it.min',
				'vendor/jquery.bootstrap-touchspin',];
		$css = ['vendor/basic',
				'vendor/dropzone',
				'vendor/lightbox/lightbox',
				'vacationsiteedit',
				'vendor/jquery.tagit',
				'vendor/tagit.ui-zendesk',
				'vendor/jquery.bootstrap-touchspin'];

		$vacationpackage = VacationPackage::findOrFail($id);
		$tags = VacationPackageTag::where('vacation_package_id','=',$id)->get();
		return view('admin.AdminVacationPackageEdit',compact('vacationpackage','tags','js','css','id'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateVacationPackage($id,Requests\AdminVacationPackageRequest $request)
	{
		$input = VacationPackage::findOrFail($id);
		$input->update($request->except('tag'));
		
		//BEGIN tag saving module
			$taginput = $request->only('tag');
			$tags = explode(",",$taginput['tag']); //separate all from comma


			foreach ($tags as $tag){
				//deleting tags that are removed
					$deltags = VacationPackageTag::where('vacation_package_id','=',$id)->get(); // ambil tag yang ada di database
					foreach($deltags as $deltag){						
						array_walk($tags, function(&$value){$value = strtolower($value);});// strtolower so checking database will be case sensitive
						
						if(!in_array(strtolower($deltag->tag->name),$tags)){ //if database tag has gone from edit form, then...
							VacationPackageTag::where('vacation_package_id','=',$id)->where('tag_id','=',$deltag->tag_id)->delete();
						}
					}

				//check if tag exist
					if ($tag!=''){ //if it is not empty
						if(Tag::where('name','like',$tag)->exists()){		
							//exist
							$existingtagid = Tag::where('name','like',$tag)->first()->id;
						}else{
							//not exist
							$existingtagid = Tag::create(['name'=>$tag])->id;
						}
					
				//check if vacationsite associated has tag or not
						if(!VacationPackageTag::where('vacation_package_id','=',$id)->where('tag_id','=',$existingtagid)->exists()){
							VacationPackageTag::create(['tag_id' => $existingtagid,'vacation_package_id' => $id,'value' => 1]); //associate new tag with the vacationsite
						}
					}
			}
		//END of tag saving module

		$input['updated_at'] = Carbon::now(); 
		return redirect('admin/vacationpackage');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteVacationPackage()
	{	
		
		$id = Request::only('id'); // mencegah request bentuk lain untuk lewat
		$user = VacationPackage::where('id',$id)->delete();
		// $user = User::destroy($id);
		return redirect('admin/vacationpackage');
	}
}
