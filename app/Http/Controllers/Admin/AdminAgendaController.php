<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Request;
use App\Models\Agenda;
use App\Models\User;
use App\Models\Vacationsite;

class AdminAgendaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	public function userAgenda($id)
	{	
		$css = ['agendalist'];
		$agendas = User::find($id)->agendas;
		$user = User::find($id);
		$name = $user->firstname." ".$user->lastname;
		return view('admin.AdminUserAgendaList',compact('agendas','name','css'));
	}


	public function agendaDetails($id){
		$css = ['agendalist'];
		$agenda = Agenda::find($id);
		$agendadetails = $agenda->agenda_details;
		$agendatitle = $agenda->name;
		return view('admin.AdminUserAgendaDetailShow', compact('agendadetails','agendatitle','css'));
	}

	public function deleteAgenda(){
		$id = Request::only('agendaidtodelete');
		$deleted = Agenda::find($id)->delete();
		return $deleted;
	}
}
