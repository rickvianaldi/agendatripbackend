<?php namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;

use App\Models\Tag;

class AdminTagsController extends Controller {

	//show tag
	public function getTags()
	{	
		$perpage = Request::get('perpage');
		if($perpage < 5 || $perpage > 100){
			$perpage = 10;
		}

	//if there is search, then do search
		$query = Request::get('search');
		if(!empty($query)){
			$tags= Tag::where('name','like', '%'.$query.'%')->paginate($perpage);
		}else{
			$tags = Tag::latest('id')->paginate($perpage);
		}
		$search['string'] = $query;

		return view('admin.AdminTagListShow',compact('tags','search'));
	}


	public function getTag($id) //individual data
	{
		$tag = Tag::findOrFail($id);
		return view('admin.AdminTagDataShow',compact('tag'));
	}
	//end of show tag



	//create tag
	public function createTag()
	{
		return view('admin.AdminTagCreate');
	}

	public function storeTag(Requests\AdminTag $request)
	{


		$input = $request->only('name');

		// dd($input);
	 	Tag::create([
			'name'=>$input['name']
			]);

		return redirect('admin/tag');
		
	}
	//end of create tag


	public function updateTag($id,Requests\AdminTag $request)
	{
		$input = Tag::findOrFail($id);
		$input->update($request->only(['name']));
		return redirect('admin/tag');
	}

	public function deleteTag()
	{	
		
		$id = Request::only('id'); // mencegah request bentuk lain untuk lewat
		$user = Tag::where('id',$id)->delete();
		// $user = User::destroy($id);
		return redirect('admin/tag');
	}


}
