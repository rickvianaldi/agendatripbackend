<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class AdminVacationSiteRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(Auth::User()){
			return true;
		}else{
			return view('Unauthorized, please contact tech support');
		}
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|min:3|regex:/^[a-zA-Z0-9 ()."]*$/',
			'address' => 'required|min:10',
			'contactnumber' =>'required',
			'description'=>'required|min:10'
		];
	}

}
