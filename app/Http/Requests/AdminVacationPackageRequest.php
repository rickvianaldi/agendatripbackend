<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
class AdminVacationPackageRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(Auth::User()){
			return true;
		}else{
			return view('Unauthorized, please contact tech support');
		}
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required|min:3|regex:/^[a-zA-Z0-9-(), ]*$/',
			'quota' =>'required|min:0',
			'header' => '',
			'price' => 'required|numeric',

			'sale_start' => 'required',
			'sale_end' => 'required|after:sale_start',

			'date_start' => 'required|after:sale_end',
			'date_end' => 'required|after:date_start',

			'facility_inclusion' => 'min:10',
			'facility_exclusion' => 'min:10',

			'itinerary' => 'min:10',
			'notes' => '',
			'terms' => '',

		];
	}

}
