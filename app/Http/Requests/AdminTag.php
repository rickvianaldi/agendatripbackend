<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use App\Models\Tag;
class AdminTag extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(Auth::User()){
			return true;
		}else{
			return view('Unauthorized, please contact tech support');
		}
	}

	public function messages(){
		return ['name.unique'=>'This Tag is exist'];
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			'name'=>'required|regex:/^[a-zA-Z0-9 ]*$/|unique:tags,name,'.$this->segment(4)
			];
	}

}
