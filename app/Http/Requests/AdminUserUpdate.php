<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class AdminUserUpdate extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(Auth::User()){
			return true;
		}else{
			return view('Unauthorized, please contact tech support');
		}
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'password' => 'min:8',
			'firstname' => 'required_if:lastname,',
			'lastname' => 'required_if:firstname,',
			'email' => 'required|email'
		];
	}

}
