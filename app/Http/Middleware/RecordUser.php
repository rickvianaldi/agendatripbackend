<?php namespace App\Http\Middleware;

use Closure;
use Hash;
use Session;
use Log;
use App\Facades\UserAuth;
use App\Models\User;
class RecordUser {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$input = $request->only('userID','key');
		$u = User::where(['id' => $input['userID']])->first();
		if($u && $u->app_key==$input['key']){
			Session::flash('user',$u);
		}
		return $next($request);
	}

}
