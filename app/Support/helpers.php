<?php 
	function return_401(){
		return response(['status' => 'ERROR' , 'message' => 'AUTH_FAILURE', 'payload' => '' ],401,['WWW-Authenticate' => 'Basic realm="fake"']);
	}

	function return_404($object_name = "Object"){
		return response(['status' => 'ERROR' , 'message' => $object_name." Not found",'payload' => ''],404);
	}
?>